CREATE VIEW `viewBitacora`
AS
SELECT
  `IdBitacora`,
  `lineaModificada`,
  `Modificacion`,
  `Fecha`,
  `User`
FROM `Bitacora`;

CREATE VIEW `viewCanton`
AS
SELECT
  `IdCanton`,
  `FkProvincia`,
  `Nombre`
FROM `Canton`;

CREATE VIEW `viewCentroAcopio`
AS
SELECT
  `IdCentroAcopio`,
  `Nombre`,
  `FkAdministrador`,
  `Longitud`,
  `Latitud`
FROM `CentroAcopio`;

CREATE VIEW `viewComercioAutorizado`
AS
SELECT
  `IdComercioAutorizado`,
  `Nombre`,
  `FkTipoComercio`,
  `FkAdministrador`
FROM `ComercioAutorizado`;

CREATE VIEW `viewDistrito`
AS
SELECT
  `IdDistrito`,
  `FkCanton`,
  `Nombre`
FROM `Distrito`;

CREATE VIEW `viewMaterial`
AS
SELECT
  `IdMaterial`,
  `Descripcion`,
  `Valor`,
  `FkTipoUnidad`
FROM `Material`;

CREATE VIEW `viewPais`
AS
SELECT
  `IdPais`,
  `Nombre`
FROM `Pais`;

CREATE VIEW `viewPersona`
AS
SELECT
  `IdPersona`,
  `Nombre`,
  `Apellido`,
  `FechaNacimiento`,
  `Foto`,
  `FkDistrito`,
  `Direccion`
FROM `Persona`;

CREATE VIEW `viewProducto`
AS
SELECT
  `IdProducto`,
  `Descripcion`,
  `Foto`
FROM `Producto`;

CREATE VIEW `viewProductoxComercio`
AS
SELECT
  `IdProductoxComercio`,
  `FkProducto`,
  `FkComericio`,
  `Valor`
FROM `ProductoxComercio`;

CREATE VIEW `viewProvincia`
AS
SELECT
  `IdProvincia`,
  `FkPais`,
  `Nombre`
FROM `Provincia`;

CREATE VIEW `viewPuntosAcumulados`
AS
SELECT
  `IdPuntos Acumulados`,
  `FkUsuario`,
  `FkMaterial`,
  `FkCentroAcopio`,
  `Cantidad`,
  `Fecha`
FROM `Puntos Acumulados`;

CREATE VIEW `viewPuntosRedimidos`
AS
SELECT
  `IdPuntosRedimidos`,
  `FkUsuario`,
  `FkProducto`,
  `FkComercioAutorizado`,
  `Cantidad`,
  `Fecha`
FROM `PuntosRedimidos`;

CREATE VIEW `viewTelefono`
AS
SELECT
  `IdTelefono`,
  `FkPersona`,
  `Numero`
FROM `Telefono`;

CREATE VIEW `viewTipoComercio`
AS
SELECT
  `IdTipoComercio`,
  `Nombre`
FROM `TipoComercio`;

CREATE VIEW `viewTipoUnidad`
AS
SELECT
  `IdTipoUnidad`,
  `Descripcion`
FROM `TipoUnidad`;

CREATE VIEW `viewUsuario`
AS
SELECT
  `IdUsuario`,
  `UserName`,
  `Email`,
  `Password`,
  `PuntosDisponibles`,
  `FkPersona`,
  `IsAdmin`
FROM `Usuario`;