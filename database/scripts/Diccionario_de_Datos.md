# Schema documentation

Generated by MySQL Workbench Model Documentation v1.0.0 - Copyright (c) 2015 Hieu Le

## Table: `Bitacora`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdBitacora` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `Modificacion` | VARCHAR(50) | Not null |   |   |
| `Fecha` | DATETIME |  | `CURRENT_TIMESTAMP` |   |
| `usuario` | VARCHAR(50) |  | `NULL` |   |
| `lineaModificada` | VARCHAR(1000) |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdBitacora` | PRIMARY |   |
| idtable1_UNIQUE | `IdBitacora` | UNIQUE |   |


## Table: `Canton`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdCanton` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `FkProvincia` | INT | Not null |   |  **foreign key** to column `IdProvincia` on table `Provincia`. |
| `Nombre` | VARCHAR(50) | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdCanton` | PRIMARY |   |
| IdCanton_UNIQUE | `IdCanton` | UNIQUE |   |
| FkCantonProvincia_idx | `FkProvincia` | INDEX |   |


## Table: `CentroAcopio`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdCentroAcopio` | INT | PRIMARY, Auto increments, Not null |   |   |
| `Nombre` | VARCHAR(50) | Not null |   |   |
| `FkAdministrador` | INT | Not null |   |  **foreign key** to column `IdUsuario` on table `Usuario`. |
| `Longitud` | FLOAT | Not null |   |   |
| `Latitud` | FLOAT | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdCentroAcopio` | PRIMARY |   |
| FkCentroUsuario_idx | `FkAdministrador` | INDEX |   |


## Table: `ComercioAutorizado`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdComercioAutorizado` | INT | PRIMARY, Auto increments, Not null |   |   |
| `Nombre` | VARCHAR(50) | Not null |   |   |
| `FkTipoComercio` | INT | Not null |   |  **foreign key** to column `IdTipoComercio` on table `TipoComercio`. |
| `FkAdministrador` | INT | Not null |   |  **foreign key** to column `IdUsuario` on table `Usuario`. |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdComercioAutorizado` | PRIMARY |   |
| FkComercioTipoComercio_idx | `FkTipoComercio` | INDEX |   |
| FkComercioUsuario_idx | `FkAdministrador` | INDEX |   |


## Table: `Distrito`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdDistrito` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `FkCanton` | INT | Not null |   |  **foreign key** to column `IdCanton` on table `Canton`. |
| `Nombre` | VARCHAR(50) | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdDistrito` | PRIMARY |   |
| idDistrito_UNIQUE | `IdDistrito` | UNIQUE |   |
| FkDsitritoCanton_idx | `FkCanton` | INDEX |   |


## Table: `Material`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdMaterial` | INT | PRIMARY, Auto increments, Not null |   |   |
| `Descripcion` | VARCHAR(50) | Not null |   |   |
| `Valor` | INT | Not null |   |   |
| `FkTipoUnidad` | INT | Not null |   |  **foreign key** to column `IdTipoUnidad` on table `TipoUnidad`. |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdMaterial` | PRIMARY |   |
| FkMaterialTipoUnidad_idx | `FkTipoUnidad` | INDEX |   |


## Table: `Pais`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdPais` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `Nombre` | VARCHAR(50) | Not null, Unique |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdPais` | PRIMARY |   |
| Tabla1col_UNIQUE | `Nombre` | UNIQUE |   |
| IdPais_UNIQUE | `IdPais` | UNIQUE |   |


## Table: `Persona`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdPersona` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `Nombre` | VARCHAR(50) | Not null |   |   |
| `Apellido` | VARCHAR(50) | Not null |   |   |
| `FechaNacimiento` | DATE | Not null |   |   |
| `Foto` | BLOB |  | `NULL` |   |
| `FkDistrito` | INT | Not null |   |  **foreign key** to column `IdDistrito` on table `Distrito`. |
| `Direccion` | VARCHAR(150) | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdPersona` | PRIMARY |   |
| idPersona_UNIQUE | `IdPersona` | UNIQUE |   |
| FkPersonaDistrito_idx | `FkDistrito` | INDEX |   |


## Table: `Producto`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdProducto` | INT | PRIMARY, Auto increments, Not null |   |   |
| `Descripcion` | VARCHAR(50) | Not null |   |   |
| `Foto` | BLOB | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdProducto` | PRIMARY |   |


## Table: `ProductoxComercio`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdProductoxComercio` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `FkProducto` | INT | Not null |   |  **foreign key** to column `IdProducto` on table `Producto`. |
| `FkComericio` | INT | Not null |   |  **foreign key** to column `IdComercioAutorizado` on table `ComercioAutorizado`. |
| `Valor` | INT | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdProductoxComercio` | PRIMARY |   |
| IdProductoxComercio_UNIQUE | `IdProductoxComercio` | UNIQUE |   |
| FkProductoxComercioProducto_idx | `FkProducto` | INDEX |   |
| FkProductoxComercioComercio_idx | `FkComericio` | INDEX |   |


## Table: `Provincia`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdProvincia` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `FkPais` | INT | Not null |   |  **foreign key** to column `IdPais` on table `Pais`. |
| `Nombre` | VARCHAR(50) | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdProvincia` | PRIMARY |   |
| IdProvincia_UNIQUE | `IdProvincia` | UNIQUE |   |
| as_idx | `FkPais` | INDEX |   |


## Table: `PuntosAcumulados`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdPuntosAcumulados` | INT | PRIMARY, Auto increments, Not null |   |   |
| `FkUsuario` | INT | Not null |   |  **foreign key** to column `IdUsuario` on table `Usuario`. |
| `FkMaterial` | INT | Not null |   |  **foreign key** to column `IdMaterial` on table `Material`. |
| `FkCentroAcopio` | INT | Not null |   |  **foreign key** to column `IdCentroAcopio` on table `CentroAcopio`. |
| `Cantidad` | INT | Not null |   |   |
| `Fecha` | DATETIME | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdPuntosAcumulados` | PRIMARY |   |
| FkPtsAcumuladosCentroAcopio_idx | `FkCentroAcopio` | INDEX |   |
| FkPtsAcumuladosMaterial_idx | `FkMaterial` | INDEX |   |
| FkPtsAcumuladosUsuario_idx | `FkUsuario` | INDEX |   |


## Table: `PuntosRedimidos`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdPuntosRedimidos` | INT | PRIMARY, Auto increments, Not null |   |   |
| `FkUsuario` | INT | Not null |   |  **foreign key** to column `IdUsuario` on table `Usuario`. |
| `FkProducto` | INT | Not null |   |  **foreign key** to column `IdProducto` on table `Producto`. |
| `FkComercioAutorizado` | INT | Not null |   |  **foreign key** to column `IdComercioAutorizado` on table `ComercioAutorizado`. |
| `Cantidad` | INT | Not null |   |   |
| `Fecha` | DATETIME | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdPuntosRedimidos` | PRIMARY |   |
| FkPtsRedimidosUsuario_idx | `FkUsuario` | INDEX |   |
| FkPtsRedimidosComercioAutorizado_idx | `FkComercioAutorizado` | INDEX |   |
| FkPtsRedimidosProducto_idx | `FkProducto` | INDEX |   |


## Table: `Telefono`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdTelefono` | INT | PRIMARY, Auto increments, Not null |   |   |
| `FkPersona` | INT | Not null |   |  **foreign key** to column `IdPersona` on table `Persona`. |
| `Numero` | INT | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdTelefono` | PRIMARY |   |
| FkTelefonoPersona_idx | `FkPersona` | INDEX |   |


## Table: `TipoComercio`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdTipoComercio` | INT | PRIMARY, Auto increments, Not null |   |   |
| `Nombre` | VARCHAR(50) | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdTipoComercio` | PRIMARY |   |


## Table: `TipoUnidad`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdTipoUnidad` | INT | PRIMARY, Auto increments, Not null |   |   |
| `Descripcion` | VARCHAR(50) | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdTipoUnidad` | PRIMARY |   |


## Table: `Usuario`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `IdUsuario` | INT | PRIMARY, Auto increments, Not null, Unique |   |   |
| `UserName` | VARCHAR(50) | Not null, Unique |   |   |
| `Email` | VARCHAR(50) | Not null |   |   |
| `Password` | VARCHAR(150) | Not null |   |   |
| `PuntosDisponibles` | INT | Not null |   |   |
| `FkPersona` | INT | Not null |   |  **foreign key** to column `IdPersona` on table `Persona`. |
| `IsAdmin` | BIT | Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `IdUsuario` | PRIMARY |   |
| IdUsuario_UNIQUE | `IdUsuario` | UNIQUE |   |
| UserName_UNIQUE | `UserName` | UNIQUE |   |
| FkUsuarioPersona_idx | `FkPersona` | INDEX |   |


