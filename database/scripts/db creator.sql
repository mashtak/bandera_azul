SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema BanderaAzul
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `BanderaAzul` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci ;
USE `BanderaAzul` ;

-- -----------------------------------------------------
-- Table `BanderaAzul`.`Pais`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Pais` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Pais` (
  `IdPais` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`IdPais`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `Tabla1col_UNIQUE` ON `BanderaAzul`.`Pais` (`Nombre` ASC);

CREATE UNIQUE INDEX `IdPais_UNIQUE` ON `BanderaAzul`.`Pais` (`IdPais` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Provincia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Provincia` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Provincia` (
  `IdProvincia` INT NOT NULL AUTO_INCREMENT,
  `FkPais` INT NOT NULL,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`IdProvincia`),
  CONSTRAINT `FkProvinciaPais`
    FOREIGN KEY (`FkPais`)
    REFERENCES `BanderaAzul`.`Pais` (`IdPais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `as_idx` ON `BanderaAzul`.`Provincia` (`FkPais` ASC);

CREATE UNIQUE INDEX `IdProvincia_UNIQUE` ON `BanderaAzul`.`Provincia` (`IdProvincia` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Canton`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Canton` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Canton` (
  `IdCanton` INT NOT NULL AUTO_INCREMENT,
  `FkProvincia` INT NOT NULL,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`IdCanton`),
  CONSTRAINT `FkCantonProvincia`
    FOREIGN KEY (`FkProvincia`)
    REFERENCES `BanderaAzul`.`Provincia` (`IdProvincia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `IdCanton_UNIQUE` ON `BanderaAzul`.`Canton` (`IdCanton` ASC);

CREATE INDEX `FkCantonProvincia_idx` ON `BanderaAzul`.`Canton` (`FkProvincia` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Distrito`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Distrito` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Distrito` (
  `IdDistrito` INT NOT NULL AUTO_INCREMENT,
  `FkCanton` INT NOT NULL,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`IdDistrito`),
  CONSTRAINT `FkDsitritoCanton`
    FOREIGN KEY (`FkCanton`)
    REFERENCES `BanderaAzul`.`Canton` (`IdCanton`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `idDistrito_UNIQUE` ON `BanderaAzul`.`Distrito` (`IdDistrito` ASC);

CREATE INDEX `FkDsitritoCanton_idx` ON `BanderaAzul`.`Distrito` (`FkCanton` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Persona`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Persona` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Persona` (
  `IdPersona` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  `Apellido` VARCHAR(50) NOT NULL,
  `FechaNacimiento` DATE NOT NULL,
  `Foto` BLOB NULL,
  `FkDistrito` INT NOT NULL,
  `Direccion` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`IdPersona`),
  CONSTRAINT `FkPersonaDistrito`
    FOREIGN KEY (`FkDistrito`)
    REFERENCES `BanderaAzul`.`Distrito` (`IdDistrito`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `idPersona_UNIQUE` ON `BanderaAzul`.`Persona` (`IdPersona` ASC);

CREATE INDEX `FkPersonaDistrito_idx` ON `BanderaAzul`.`Persona` (`FkDistrito` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Usuario` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Usuario` (
  `IdUsuario` INT NOT NULL AUTO_INCREMENT,
  `UserName` VARCHAR(50) NOT NULL,
  `Email` VARCHAR(50) NOT NULL,
  `Password` VARCHAR(150) NOT NULL,
  `PuntosDisponibles` INT NOT NULL,
  `FkPersona` INT NOT NULL,
  `IsAdmin` BIT(1) NOT NULL,
  PRIMARY KEY (`IdUsuario`),
  CONSTRAINT `FkUsuarioPersona`
    FOREIGN KEY (`FkPersona`)
    REFERENCES `BanderaAzul`.`Persona` (`IdPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `IdUsuario_UNIQUE` ON `BanderaAzul`.`Usuario` (`IdUsuario` ASC);

CREATE UNIQUE INDEX `UserName_UNIQUE` ON `BanderaAzul`.`Usuario` (`UserName` ASC);

CREATE INDEX `FkUsuarioPersona_idx` ON `BanderaAzul`.`Usuario` (`FkPersona` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`CentroAcopio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`CentroAcopio` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`CentroAcopio` (
  `IdCentroAcopio` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  `FkAdministrador` INT NOT NULL,
  `Longitud` FLOAT NOT NULL,
  `Latitud` FLOAT NOT NULL,
  PRIMARY KEY (`IdCentroAcopio`),
  CONSTRAINT `FkCentroUsuario`
    FOREIGN KEY (`FkAdministrador`)
    REFERENCES `BanderaAzul`.`Usuario` (`IdUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FkCentroUsuario_idx` ON `BanderaAzul`.`CentroAcopio` (`FkAdministrador` ASC);



-- -----------------------------------------------------
-- Table `BanderaAzul`.`TipoComercio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`TipoComercio` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`TipoComercio` (
  `IdTipoComercio` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`IdTipoComercio`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BanderaAzul`.`ComercioAutorizado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`ComercioAutorizado` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`ComercioAutorizado` (
  `IdComercioAutorizado` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  `FkTipoComercio` INT NOT NULL,
  `FkAdministrador` INT NOT NULL,
  PRIMARY KEY (`IdComercioAutorizado`),
  CONSTRAINT `FkComercioTipoComercio`
    FOREIGN KEY (`FkTipoComercio`)
    REFERENCES `BanderaAzul`.`TipoComercio` (`IdTipoComercio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FkComercioUsuario`
    FOREIGN KEY (`FkAdministrador`)
    REFERENCES `BanderaAzul`.`Usuario` (`IdUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FkComercioTipoComercio_idx` ON `BanderaAzul`.`ComercioAutorizado` (`FkTipoComercio` ASC);

CREATE INDEX `FkComercioUsuario_idx` ON `BanderaAzul`.`ComercioAutorizado` (`FkAdministrador` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`TipoUnidad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`TipoUnidad` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`TipoUnidad` (
  `IdTipoUnidad` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`IdTipoUnidad`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Material`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Material` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Material` (
  `IdMaterial` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(50) NOT NULL,
  `Valor` INT NOT NULL,
  `FkTipoUnidad` INT NOT NULL,
  PRIMARY KEY (`IdMaterial`),
  CONSTRAINT `FkMaterialTipoUnidad`
    FOREIGN KEY (`FkTipoUnidad`)
    REFERENCES `BanderaAzul`.`TipoUnidad` (`IdTipoUnidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FkMaterialTipoUnidad_idx` ON `BanderaAzul`.`Material` (`FkTipoUnidad` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`PuntosAcumulados`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Puntos Acumulados` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`PuntosAcumulados` (
  `IdPuntosAcumulados` INT NOT NULL AUTO_INCREMENT,
  `FkUsuario` INT NOT NULL,
  `FkMaterial` INT NOT NULL,
  `FkCentroAcopio` INT NOT NULL,
  `Cantidad` INT NOT NULL,
  `Fecha` DATETIME NOT NULL,
  PRIMARY KEY (`IdPuntosAcumulados`),
  CONSTRAINT `FkPtsAcumuladosCentroAcopio`
    FOREIGN KEY (`FkCentroAcopio`)
    REFERENCES `BanderaAzul`.`CentroAcopio` (`IdCentroAcopio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FkPtsAcumuladosMaterial`
    FOREIGN KEY (`FkMaterial`)
    REFERENCES `BanderaAzul`.`Material` (`IdMaterial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FkPtsAcumuladosUsuario`
    FOREIGN KEY (`FkUsuario`)
    REFERENCES `BanderaAzul`.`Usuario` (`IdUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FkPtsAcumuladosCentroAcopio_idx` ON `BanderaAzul`.`PuntosAcumulados` (`FkCentroAcopio` ASC);

CREATE INDEX `FkPtsAcumuladosMaterial_idx` ON `BanderaAzul`.`PuntosAcumulados` (`FkMaterial` ASC);

CREATE INDEX `FkPtsAcumuladosUsuario_idx` ON `BanderaAzul`.`PuntosAcumulados` (`FkUsuario` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Telefono`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Telefono` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Telefono` (
  `IdTelefono` INT NOT NULL AUTO_INCREMENT,
  `FkPersona` INT NOT NULL,
  `Numero` INT NOT NULL,
  PRIMARY KEY (`IdTelefono`),
  CONSTRAINT `FkTelefonoPersona`
    FOREIGN KEY (`FkPersona`)
    REFERENCES `BanderaAzul`.`Persona` (`IdPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FkTelefonoPersona_idx` ON `BanderaAzul`.`Telefono` (`FkPersona` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Producto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Producto` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Producto` (
  `IdProducto` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(50) NOT NULL,
  `Foto` BLOB NOT NULL,
  PRIMARY KEY (`IdProducto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BanderaAzul`.`PuntosRedimidos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`PuntosRedimidos` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`PuntosRedimidos` (
  `IdPuntosRedimidos` INT NOT NULL AUTO_INCREMENT,
  `FkUsuario` INT NOT NULL,
  `FkProducto` INT NOT NULL,
  `FkComercioAutorizado` INT NOT NULL,
  `Cantidad` INT NOT NULL,
  `Fecha` DATETIME NOT NULL,
  PRIMARY KEY (`IdPuntosRedimidos`),
  CONSTRAINT `FkPtsRedimidosUsuario`
    FOREIGN KEY (`FkUsuario`)
    REFERENCES `BanderaAzul`.`Usuario` (`IdUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FkPtsRedimidosComercioAutorizado`
    FOREIGN KEY (`FkComercioAutorizado`)
    REFERENCES `BanderaAzul`.`ComercioAutorizado` (`IdComercioAutorizado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FkPtsRedimidosProducto`
    FOREIGN KEY (`FkProducto`)
    REFERENCES `BanderaAzul`.`Producto` (`IdProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FkPtsRedimidosUsuario_idx` ON `BanderaAzul`.`PuntosRedimidos` (`FkUsuario` ASC);

CREATE INDEX `FkPtsRedimidosComercioAutorizado_idx` ON `BanderaAzul`.`PuntosRedimidos` (`FkComercioAutorizado` ASC);

CREATE INDEX `FkPtsRedimidosProducto_idx` ON `BanderaAzul`.`PuntosRedimidos` (`FkProducto` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`ProductoxComercio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`ProductoxComercio` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`ProductoxComercio` (
  `IdProductoxComercio` INT NOT NULL,
  `FkProducto` INT NOT NULL,
  `FkComericio` INT NOT NULL,
  `Valor` INT NOT NULL,
  PRIMARY KEY (`IdProductoxComercio`),
  CONSTRAINT `FkProductoxComercioProducto`
    FOREIGN KEY (`FkProducto`)
    REFERENCES `BanderaAzul`.`Producto` (`IdProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FkProductoxComercioComercio`
    FOREIGN KEY (`FkComericio`)
    REFERENCES `BanderaAzul`.`ComercioAutorizado` (`IdComercioAutorizado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `IdProductoxComercio_UNIQUE` ON `BanderaAzul`.`ProductoxComercio` (`IdProductoxComercio` ASC);

CREATE INDEX `FkProductoxComercioProducto_idx` ON `BanderaAzul`.`ProductoxComercio` (`FkProducto` ASC);

CREATE INDEX `FkProductoxComercioComercio_idx` ON `BanderaAzul`.`ProductoxComercio` (`FkComericio` ASC);


-- -----------------------------------------------------
-- Table `BanderaAzul`.`Bitacora`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BanderaAzul`.`Bitacora` ;

CREATE TABLE IF NOT EXISTS `BanderaAzul`.`Bitacora` (
  `IdBitacora` INT NOT NULL AUTO_INCREMENT,
  `lineaModificada` INT NOT NULL,
  `Modificacion` VARCHAR(50) NOT NULL,
  `Fecha` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `User` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`IdBitacora`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `idtable1_UNIQUE` ON `BanderaAzul`.`Bitacora` (`IdBitacora` ASC);

SET SQL_MODE = '';
GRANT USAGE ON *.* TO userWeb;
DROP USER IF EXISTS userWeb;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'userWeb' IDENTIFIED with mysql_native_password BY 'userweb123';
GRANT SELECT, INSERT, TRIGGER ON TABLE `BanderaAzul`.* TO 'userWeb';
GRANT EXECUTE ON `BanderaAzul`.* TO 'userWeb';

SET SQL_MODE = '';
DROP USER IF EXISTS userAdmin;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'userAdmin' IDENTIFIED with mysql_native_password BY 'userAdmin123';
GRANT ALL ON `BanderaAzul`.* TO 'userAdmin';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE `BanderaAzul`.* TO 'userAdmin';
GRANT EXECUTE ON `BanderaAzul`.* TO 'userAdmin';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
