DROP PROCEDURE IF EXISTS `spSelectBitacora`;

DELIMITER  |
CREATE PROCEDURE `spSelectBitacora` ()
BEGIN
  SELECT
    `IdBitacora`,
    `lineaModificada`,
    `Modificacion`,
    `Fecha`,
    `User`
  FROM `Bitacora`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateBitacora`;

DELIMITER  |
CREATE PROCEDURE `spUpdateBitacora` (
  IN `pIdBitacora` int,
  IN `plineaModificada` int,
  IN `pModificacion` varchar(50),
  IN `pFecha` datetime,
  IN `pUser` varchar(50)
)
BEGIN
  UPDATE `Bitacora` SET
    `lineaModificada` = `plineaModificada`,
    `Modificacion` = `pModificacion`,
    `Fecha` = `pFecha`,
    `User` = `pUser`
  WHERE 
    (`IdBitacora` = `pIdBitacora`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertBitacora`;

DELIMITER  |
CREATE PROCEDURE `spInsertBitacora` (
  IN `plineaModificada` int,
  IN `pModificacion` varchar(50),
  IN `pFecha` datetime,
  IN `pUser` varchar(50)
)
BEGIN
  INSERT INTO `Bitacora`
  (
    `lineaModificada`,
    `Modificacion`,
    `Fecha`,
    `User`
  )
  VALUES 
  (
    `plineaModificada`,
    `pModificacion`,
    `pFecha`,
    `pUser`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteBitacora`;

DELIMITER  |
CREATE PROCEDURE `spDeleteBitacora` (
   IN 
  `pIdBitacora`
   int
)
BEGIN
  DELETE FROM `Bitacora`
  WHERE     
    (`IdBitacora` = `pIdBitacora`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectCanton`;

DELIMITER  |
CREATE PROCEDURE `spSelectCanton` ()
BEGIN
  SELECT
    `IdCanton`,
    `FkProvincia`,
    `Nombre`
  FROM `Canton`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateCanton`;

DELIMITER  |
CREATE PROCEDURE `spUpdateCanton` (
  IN `pIdCanton` int,
  IN `pFkProvincia` int,
  IN `pNombre` varchar(50)
)
BEGIN
  UPDATE `Canton` SET
    `FkProvincia` = `pFkProvincia`,
    `Nombre` = `pNombre`
  WHERE 
    (`IdCanton` = `pIdCanton`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertCanton`;

DELIMITER  |
CREATE PROCEDURE `spInsertCanton` (
  IN `pFkProvincia` int,
  IN `pNombre` varchar(50)
)
BEGIN
  INSERT INTO `Canton`
  (
    `FkProvincia`,
    `Nombre`
  )
  VALUES 
  (
    `pFkProvincia`,
    `pNombre`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteCanton`;

DELIMITER  |
CREATE PROCEDURE `spDeleteCanton` (
   IN 
  `pIdCanton`
   int
)
BEGIN
  DELETE FROM `Canton`
  WHERE     
    (`IdCanton` = `pIdCanton`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectCentroAcopio`;

DELIMITER  |
CREATE PROCEDURE `spSelectCentroAcopio` ()
BEGIN
  SELECT
    `IdCentroAcopio`,
    `Nombre`,
    `Longitud`,
    `Latitud`
  FROM `CentroAcopio`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectCentroAcopioByUserAdmin`;

DELIMITER  |
CREATE PROCEDURE `spSelectCentroAcopioByUserAdmin` (
 IN id_usuario int
)
BEGIN
  SELECT
    `IdCentroAcopio`,
    `Nombre`,
    `Longitud`,
    `Latitud`,
    `FkAdministrador`
  FROM `CentroAcopio`
  where `FkAdministrador` = id_usuario;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spFindCentroAcopio`;

DELIMITER  |
CREATE PROCEDURE `spFindCentroAcopio` (
  IN `pIdCentroAcopio` int
)
BEGIN
  SELECT
    `Nombre`,
    `FkAdministrador`,
    `Longitud`,
    `Latitud`
  From `CentroAcopio`
  WHERE
    (`IdCentroAcopio` = `pIdCentroAcopio`);
END|
DELIMITER ;



DROP PROCEDURE IF EXISTS `spUpdateCentroAcopio`;

DELIMITER  |
CREATE PROCEDURE `spUpdateCentroAcopio` (
  IN `pIdCentroAcopio` int,
  IN `pNombre` varchar(50),
  IN `pLongitud` float,
  IN `pLatitud` float
)
BEGIN
  UPDATE `CentroAcopio` SET
    `Nombre` = `pNombre`,
    `Longitud` = `pLongitud`,
    `Latitud` = `pLatitud`
  WHERE 
    (`IdCentroAcopio` = `pIdCentroAcopio`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertCentroAcopio`;

DELIMITER  |
CREATE PROCEDURE `spInsertCentroAcopio` (
  IN `pNombre` varchar(50),
  IN `pLongitud` float,
  IN `pLatitud` float,
  IN `pFkAdministrador` int
)
BEGIN
  INSERT INTO `CentroAcopio`
  (
    `Nombre`,
    `Longitud`,
    `Latitud`,
    `FkAdministrador`
  )
  VALUES 
  (
    `pNombre`,
    `pLongitud`,
    `pLatitud`,
    `pFkAdministrador`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteCentroAcopio`;

DELIMITER  |
CREATE PROCEDURE `spDeleteCentroAcopio` (
   IN 
  `pIdCentroAcopio`
   int
)
BEGIN
  DELETE FROM `CentroAcopio`
  WHERE     
    (`IdCentroAcopio` = `pIdCentroAcopio`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectComercioAutorizado`;

DELIMITER  |
CREATE PROCEDURE `spSelectComercioAutorizado` ()
BEGIN
  SELECT
    `IdComercioAutorizado`,
    `Nombre`,
    `FkTipoComercio`
  FROM `ComercioAutorizado`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectComercioAutorizadoByUserAdmin`;

DELIMITER  |
CREATE PROCEDURE `spSelectComercioAutorizadoByUserAdmin` (
IN `id_usuario` int
)
BEGIN
  SELECT
    `IdComercioAutorizado`,
    `Nombre`,
    `FkTipoComercio`,
     `FkAdministrador`
  FROM `ComercioAutorizado`
    WHERE `FkAdministrador`=`id_usuario`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateComercioAutorizado`;

DELIMITER  |
CREATE PROCEDURE `spUpdateComercioAutorizado` (
  IN `pIdComercioAutorizado` int,
  IN `pNombre` varchar(50),
  IN `pFkTipoComercio` int
)
BEGIN
  UPDATE `ComercioAutorizado` SET
    `Nombre` = `pNombre`,
    `FkTipoComercio` = `pFkTipoComercio`
  WHERE 
    (`IdComercioAutorizado` = `pIdComercioAutorizado`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertComercioAutorizado`;

DELIMITER  |
CREATE PROCEDURE `spInsertComercioAutorizado` (
  IN `pNombre` varchar(50),
  IN `pFkTipoComercio` int,
  IN `pFkAdministrador`
)
BEGIN
  INSERT INTO `ComercioAutorizado`
  (
    `Nombre`,
    `FkTipoComercio`,
    `FkAdministrador`
  )
  VALUES 
  (
    `pNombre`,
    `pFkTipoComercio`,
    `pFkAdministrador`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteComercioAutorizado`;

DELIMITER  |
CREATE PROCEDURE `spDeleteComercioAutorizado` (
   IN 
  `pIdComercioAutorizado`
   int
)
BEGIN
  DELETE FROM `ComercioAutorizado`
  WHERE     
    (`IdComercioAutorizado` = `pIdComercioAutorizado`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectDistrito`;

DELIMITER  |
CREATE PROCEDURE `spSelectDistrito` ()
BEGIN
  SELECT
    `IdDistrito`,
    `FkCanton`,
    `Nombre`
  FROM `Distrito`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateDistrito`;

DELIMITER  |
CREATE PROCEDURE `spUpdateDistrito` (
  IN `pIdDistrito` int,
  IN `pFkCanton` int,
  IN `pNombre` varchar(50)
)
BEGIN
  UPDATE `Distrito` SET
    `FkCanton` = `pFkCanton`,
    `Nombre` = `pNombre`
  WHERE 
    (`IdDistrito` = `pIdDistrito`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertDistrito`;

DELIMITER  |
CREATE PROCEDURE `spInsertDistrito` (
  IN `pFkCanton` int,
  IN `pNombre` varchar(50)
)
BEGIN
  INSERT INTO `Distrito`
  (
    `FkCanton`,
    `Nombre`
  )
  VALUES 
  (
    `pFkCanton`,
    `pNombre`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteDistrito`;

DELIMITER  |
CREATE PROCEDURE `spDeleteDistrito` (
   IN 
  `pIdDistrito`
   int
)
BEGIN
  DELETE FROM `Distrito`
  WHERE     
    (`IdDistrito` = `pIdDistrito`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectMaterial`;

DELIMITER  |
CREATE PROCEDURE `spSelectMaterial` ()
BEGIN
  SELECT
    `IdMaterial`,
    `Descripcion`,
    `Valor`,
    `FkTipoUnidad`
  FROM `Material`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateMaterial`;

DELIMITER  |
CREATE PROCEDURE `spUpdateMaterial` (
  IN `pIdMaterial` int,
  IN `pDescripcion` varchar(50),
  IN `pValor` int,
  IN `pFkTipoUnidad` int
)
BEGIN
  UPDATE `Material` SET
    `Descripcion` = `pDescripcion`,
    `Valor` = `pValor`,
    `FkTipoUnidad` = `pFkTipoUnidad`
  WHERE 
    (`IdMaterial` = `pIdMaterial`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertMaterial`;

DELIMITER  |
CREATE PROCEDURE `spInsertMaterial` (
  IN `pDescripcion` varchar(50),
  IN `pValor` int,
  IN `pFkTipoUnidad` int
)
BEGIN
  INSERT INTO `Material`
  (
    `Descripcion`,
    `Valor`,
    `FkTipoUnidad`
  )
  VALUES 
  (
    `pDescripcion`,
    `pValor`,
    `pFkTipoUnidad`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteMaterial`;

DELIMITER  |
CREATE PROCEDURE `spDeleteMaterial` (
   IN 
  `pIdMaterial`
   int
)
BEGIN
  DELETE FROM `Material`
  WHERE     
    (`IdMaterial` = `pIdMaterial`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectPais`;

DELIMITER  |
CREATE PROCEDURE `spSelectPais` ()
BEGIN
  SELECT
    `IdPais`,
    `Nombre`
  FROM `Pais`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdatePais`;

DELIMITER  |
CREATE PROCEDURE `spUpdatePais` (
  IN `pIdPais` int,
  IN `pNombre` varchar(50)
)
BEGIN
  UPDATE `Pais` SET
    `Nombre` = `pNombre`
  WHERE 
    (`IdPais` = `pIdPais`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertPais`;

DELIMITER  |
CREATE PROCEDURE `spInsertPais` (
  IN `pNombre` varchar(50)
)
BEGIN
  INSERT INTO `Pais`
  (
    `Nombre`
  )
  VALUES 
  (
    `pNombre`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeletePais`;

DELIMITER  |
CREATE PROCEDURE `spDeletePais` (
   IN 
  `pIdPais`
   int
)
BEGIN
  DELETE FROM `Pais`
  WHERE     
    (`IdPais` = `pIdPais`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectPersona`;

DELIMITER  |
CREATE PROCEDURE `spSelectPersona` ()
BEGIN
  SELECT
    `IdPersona`,
    `Nombre`,
    `Apellido`,
    `FechaNacimiento`,
    `Foto`,
    `FkDistrito`,
    `Direccion`
  FROM `Persona`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectPersonaWithoutImage`;

DELIMITER  |
CREATE PROCEDURE `spSelectPersonaWithoutImage` ()
BEGIN
  SELECT
    `IdPersona`,
    `Nombre`,
    `Apellido`,
    `FechaNacimiento`,
    `FkDistrito`,
    `Direccion`
  FROM `Persona`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdatePersona`;

DELIMITER  |
CREATE PROCEDURE `spUpdatePersona` (
  IN `pIdPersona` int,
  IN `pNombre` varchar(50),
  IN `pApellido` varchar(50),
  IN `pFechaNacimiento` date,
  IN `pFoto` blob,
  IN `pFkDistrito` int,
  IN `pDireccion` varchar(150)
)
BEGIN
  UPDATE `Persona` SET
    `Nombre` = `pNombre`,
    `Apellido` = `pApellido`,
    `FechaNacimiento` = `pFechaNacimiento`,
    `Foto` = `pFoto`,
    `FkDistrito` = `pFkDistrito`,
    `Direccion` = `pDireccion`
  WHERE 
    (`IdPersona` = `pIdPersona`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertPersona`;

DELIMITER  |
CREATE PROCEDURE `spInsertPersona` (
  IN `pNombre` varchar(50),
  IN `pApellido` varchar(50),
  IN `pFechaNacimiento` date,
  IN `pFoto` blob,
  IN `pFkDistrito` int,
  IN `pDireccion` varchar(150)
)
BEGIN
  INSERT INTO `Persona`
  (
    `Nombre`,
    `Apellido`,
    `FechaNacimiento`,
    `Foto`,
    `FkDistrito`,
    `Direccion`
  )
  VALUES 
  (
    `pNombre`,
    `pApellido`,
    `pFechaNacimiento`,
    `pFoto`,
    `pFkDistrito`,
    `pDireccion`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeletePersona`;

DELIMITER  |
CREATE PROCEDURE `spDeletePersona` (
   IN 
  `pIdPersona`
   int
)
BEGIN
  DELETE FROM `Persona`
  WHERE     
    (`IdPersona` = `pIdPersona`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectProducto`;

DELIMITER  |
CREATE PROCEDURE `spSelectProducto` ()
BEGIN
  SELECT
    `IdProducto`,
    `Descripcion`,
    `Foto`
  FROM `Producto`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateProducto`;

DELIMITER  |
CREATE PROCEDURE `spUpdateProducto` (
  IN `pIdProducto` int,
  IN `pDescripcion` varchar(50),
  IN `pFoto` blob
)
BEGIN
  UPDATE `Producto` SET
    `Descripcion` = `pDescripcion`,
    `Foto` = `pFoto`
  WHERE 
    (`IdProducto` = `pIdProducto`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertProducto`;

DELIMITER  |
CREATE PROCEDURE `spInsertProducto` (
  IN `pDescripcion` varchar(50),
  IN `pFoto` blob
)
BEGIN
  INSERT INTO `Producto`
  (
    `Descripcion`,
    `Foto`
  )
  VALUES 
  (
    `pDescripcion`,
    `pFoto`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteProducto`;

DELIMITER  |
CREATE PROCEDURE `spDeleteProducto` (
   IN 
  `pIdProducto`
   int
)
BEGIN
  DELETE FROM `Producto`
  WHERE     
    (`IdProducto` = `pIdProducto`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectProductoxComercio`;

DELIMITER  |
CREATE PROCEDURE `spSelectProductoxComercio` ()
BEGIN
  SELECT
    `IdProductoxComercio`,
    `FkProducto`,
    `FkComericio`,
    `Valor`
  FROM `ProductoxComercio`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateProductoxComercio`;

DELIMITER  |
CREATE PROCEDURE `spUpdateProductoxComercio` (
  IN `pIdProductoxComercio` int,
  IN `pFkProducto` int,
  IN `pFkComericio` int,
  IN `pValor` int
)
BEGIN
  UPDATE `ProductoxComercio` SET
    `FkProducto` = `pFkProducto`,
    `FkComericio` = `pFkComericio`,
    `Valor` = `pValor`
  WHERE 
    (`IdProductoxComercio` = `pIdProductoxComercio`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertProductoxComercio`;

DELIMITER  |
CREATE PROCEDURE `spInsertProductoxComercio` (
  IN `pFkProducto` int,
  IN `pFkComericio` int,
  IN `pValor` int
)
BEGIN
  INSERT INTO `ProductoxComercio`
  (
    `FkProducto`,
    `FkComericio`,
    `Valor`
  )
  VALUES 
  (
    `pFkProducto`,
    `pFkComericio`,
    `pValor`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteProductoxComercio`;

DELIMITER  |
CREATE PROCEDURE `spDeleteProductoxComercio` (
   IN 
  `pIdProductoxComercio`
   int
)
BEGIN
  DELETE FROM `ProductoxComercio`
  WHERE     
    (`IdProductoxComercio` = `pIdProductoxComercio`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spInsertPoductoConComercio`;
DELIMITER  |
CREATE PROCEDURE `spInsertPoductoConComercio`(
  IN `pDescripcion` varchar(50),
  IN `pFoto` blob,
  IN `pFkComericio` int,
  IN `pValor` int
)
BEGIN
  DECLARE id_product INT;
  START TRANSACTION;
    call spInsertProducto(`pDescripcion`,`pFoto`);
    SELECT MAX(IdProducto) INTO id_product FROM `Producto`;
    call spInsertProductoxComercio(`id_product`,`pFkComericio`,`pValor`);
  COMMIT;

END|
DELIMITER ;


DROP PROCEDURE IF EXISTS `spSelectProvincia`;

DELIMITER  |
CREATE PROCEDURE `spSelectProvincia` ()
BEGIN
  SELECT
    `IdProvincia`,
    `FkPais`,
    `Nombre`
  FROM `Provincia`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateProvincia`;

DELIMITER  |
CREATE PROCEDURE `spUpdateProvincia` (
  IN `pIdProvincia` int,
  IN `pFkPais` int,
  IN `pNombre` varchar(50)
)
BEGIN
  UPDATE `Provincia` SET
    `FkPais` = `pFkPais`,
    `Nombre` = `pNombre`
  WHERE 
    (`IdProvincia` = `pIdProvincia`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertProvincia`;

DELIMITER  |
CREATE PROCEDURE `spInsertProvincia` (
  IN `pFkPais` int,
  IN `pNombre` varchar(50)
)
BEGIN
  INSERT INTO `Provincia`
  (
    `FkPais`,
    `Nombre`
  )
  VALUES 
  (
    `pFkPais`,
    `pNombre`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteProvincia`;

DELIMITER  |
CREATE PROCEDURE `spDeleteProvincia` (
   IN 
  `pIdProvincia`
   int
)
BEGIN
  DELETE FROM `Provincia`
  WHERE     
    (`IdProvincia` = `pIdProvincia`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectPuntosAcumulados`;

DELIMITER  |
CREATE PROCEDURE `spSelectPuntosAcumulados` ()
BEGIN
  SELECT
    `IdPuntosAcumulados`,
    `FkUsuario`,
    `FkMaterial`,
    `FkCentroAcopio`,
    `Cantidad`,
    `Fecha`
  FROM `PuntosAcumulados`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdatePuntos Acumulados`;

DELIMITER  |
CREATE PROCEDURE `spUpdatePuntos Acumulados` (
  IN `pIdPuntos Acumulados` int,
  IN `pFkUsuario` int,
  IN `pFkMaterial` int,
  IN `pFkCentroAcopio` int,
  IN `pCantidad` int,
  IN `pFecha` datetime
)
BEGIN
  UPDATE `Puntos Acumulados` SET
    `FkUsuario` = `pFkUsuario`,
    `FkMaterial` = `pFkMaterial`,
    `FkCentroAcopio` = `pFkCentroAcopio`,
    `Cantidad` = `pCantidad`,
    `Fecha` = `pFecha`
  WHERE 
    (`IdPuntos Acumulados` = `pIdPuntos Acumulados`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertPuntosAcumulados`;
DELIMITER  |
CREATE PROCEDURE `spInsertPuntosAcumulados` (
  IN `pFkUsuario` int,
  IN `pFkMaterial` int,
  IN `pFkCentroAcopio` int,
  IN `pCantidad` int,
  IN `pFecha` datetime
)
BEGIN
  INSERT INTO `PuntosAcumulados`
  (
    `FkUsuario`,
    `FkMaterial`,
    `FkCentroAcopio`,
    `Cantidad`,
    `Fecha`
  )
  VALUES 
  (
    `pFkUsuario`,
    `pFkMaterial`,
    `pFkCentroAcopio`,
    `pCantidad`,
    `pFecha`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeletePuntos Acumulados`;

DELIMITER  |
CREATE PROCEDURE `spDeletePuntos Acumulados` (
   IN 
  `pIdPuntos Acumulados`
   int
)
BEGIN
  DELETE FROM `Puntos Acumulados`
  WHERE     
    (`IdPuntos Acumulados` = `pIdPuntos Acumulados`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectPuntosRedimidos`;

DELIMITER  |
CREATE PROCEDURE `spSelectPuntosRedimidos` ()
BEGIN
  SELECT
    `IdPuntosRedimidos`,
    `FkUsuario`,
    `FkProducto`,
    `FkComercioAutorizado`,
    `Cantidad`,
    `Fecha`
  FROM `PuntosRedimidos`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdatePuntosRedimidos`;

DELIMITER  |
CREATE PROCEDURE `spUpdatePuntosRedimidos` (
  IN `pIdPuntosRedimidos` int,
  IN `pFkUsuario` int,
  IN `pFkProducto` int,
  IN `pFkComercioAutorizado` int,
  IN `pCantidad` int,
  IN `pFecha` datetime
)
BEGIN
  UPDATE `PuntosRedimidos` SET
    `FkUsuario` = `pFkUsuario`,
    `FkProducto` = `pFkProducto`,
    `FkComercioAutorizado` = `pFkComercioAutorizado`,
    `Cantidad` = `pCantidad`,
    `Fecha` = `pFecha`
  WHERE 
    (`IdPuntosRedimidos` = `pIdPuntosRedimidos`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertPuntosRedimidos`;

DELIMITER  |
CREATE PROCEDURE `spInsertPuntosRedimidos` (
  IN `pFkUsuario` int,
  IN `pFkProducto` int,
  IN `pFkComercioAutorizado` int,
  IN `pCantidad` int,
  IN `pFecha` datetime
)
BEGIN
  INSERT INTO `PuntosRedimidos`
  (
    `FkUsuario`,
    `FkProducto`,
    `FkComercioAutorizado`,
    `Cantidad`,
    `Fecha`
  )
  VALUES 
  (
    `pFkUsuario`,
    `pFkProducto`,
    `pFkComercioAutorizado`,
    `pCantidad`,
    `pFecha`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeletePuntosRedimidos`;

DELIMITER  |
CREATE PROCEDURE `spDeletePuntosRedimidos` (
   IN 
  `pIdPuntosRedimidos`
   int
)
BEGIN
  DELETE FROM `PuntosRedimidos`
  WHERE     
    (`IdPuntosRedimidos` = `pIdPuntosRedimidos`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectTelefono`;

DELIMITER  |
CREATE PROCEDURE `spSelectTelefono` ()
BEGIN
  SELECT
    `IdTelefono`,
    `FkPersona`,
    `Numero`
  FROM `Telefono`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateTelefono`;

DELIMITER  |
CREATE PROCEDURE `spUpdateTelefono` (
  IN `pIdTelefono` int,
  IN `pFkPersona` int,
  IN `pNumero` int
)
BEGIN
  UPDATE `Telefono` SET
    `FkPersona` = `pFkPersona`,
    `Numero` = `pNumero`
  WHERE 
    (`IdTelefono` = `pIdTelefono`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertTelefono`;

DELIMITER  |
CREATE PROCEDURE `spInsertTelefono` (
  IN `pFkPersona` int,
  IN `pNumero` int
)
BEGIN
  INSERT INTO `Telefono`
  (
    `FkPersona`,
    `Numero`
  )
  VALUES 
  (
    `pFkPersona`,
    `pNumero`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteTelefono`;

DELIMITER  |
CREATE PROCEDURE `spDeleteTelefono` (
   IN 
  `pIdTelefono`
   int
)
BEGIN
  DELETE FROM `Telefono`
  WHERE     
    (`IdTelefono` = `pIdTelefono`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectTipoComercio`;

DELIMITER  |
CREATE PROCEDURE `spSelectTipoComercio` ()
BEGIN
  SELECT
    `IdTipoComercio`,
    `Nombre`
  FROM `TipoComercio`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateTipoComercio`;

DELIMITER  |
CREATE PROCEDURE `spUpdateTipoComercio` (
  IN `pIdTipoComercio` int,
  IN `pNombre` varchar(50)
)
BEGIN
  UPDATE `TipoComercio` SET
    `Nombre` = `pNombre`
  WHERE 
    (`IdTipoComercio` = `pIdTipoComercio`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertTipoComercio`;

DELIMITER  |
CREATE PROCEDURE `spInsertTipoComercio` (
  IN `pNombre` varchar(50)
)
BEGIN
  INSERT INTO `TipoComercio`
  (
    `Nombre`
  )
  VALUES 
  (
    `pNombre`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteTipoComercio`;

DELIMITER  |
CREATE PROCEDURE `spDeleteTipoComercio` (
   IN 
  `pIdTipoComercio`
   int
)
BEGIN
  DELETE FROM `TipoComercio`
  WHERE     
    (`IdTipoComercio` = `pIdTipoComercio`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectTipoUnidad`;

DELIMITER  |
CREATE PROCEDURE `spSelectTipoUnidad` ()
BEGIN
  SELECT
    `IdTipoUnidad`,
    `Descripcion`
  FROM `TipoUnidad`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateTipoUnidad`;

DELIMITER  |
CREATE PROCEDURE `spUpdateTipoUnidad` (
  IN `pIdTipoUnidad` int,
  IN `pDescripcion` varchar(50)
)
BEGIN
  UPDATE `TipoUnidad` SET
    `Descripcion` = `pDescripcion`
  WHERE 
    (`IdTipoUnidad` = `pIdTipoUnidad`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertTipoUnidad`;

DELIMITER  |
CREATE PROCEDURE `spInsertTipoUnidad` (
  IN `pDescripcion` varchar(50)
)
BEGIN
  INSERT INTO `TipoUnidad`
  (
    `Descripcion`
  )
  VALUES 
  (
    `pDescripcion`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteTipoUnidad`;

DELIMITER  |
CREATE PROCEDURE `spDeleteTipoUnidad` (
   IN 
  `pIdTipoUnidad`
   int
)
BEGIN
  DELETE FROM `TipoUnidad`
  WHERE     
    (`IdTipoUnidad` = `pIdTipoUnidad`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spSelectUsuario`;

DELIMITER  |
CREATE PROCEDURE `spSelectUsuario` ()
BEGIN
  SELECT
    `IdUsuario`,
    `UserName`,
    `Email`,
    `Password`,
    `PuntosDisponibles`,
    `FkPersona`,
    `IsAdmin`
  FROM `Usuario`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spFindUsuario`;

DELIMITER  |
CREATE PROCEDURE `spFindUsuario` (
  IN `pUserName` varchar(50)
)
BEGIN
  SELECT
    `IdUsuario`,
    `UserName`,
    `Email`,
    `Password`,
    `PuntosDisponibles`,
    `FkPersona`,
    `IsAdmin`
  FROM `Usuario`
  WHERE `UserName`=`pUserName`;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateUsuario`;

DELIMITER  |
CREATE PROCEDURE `spUpdateUsuario` (
  IN `pIdUsuario` int,
  IN `pUserName` varchar(50),
  IN `pEmail` varchar(50),
  IN `pPassword` varchar(150),
  IN `pPuntosDisponibles` int,
  IN `pFkPersona` int,
  IN `pIsAdmin` bit
)
BEGIN
  UPDATE `Usuario` SET
    `UserName` = `pUserName`,
    `Email` = `pEmail`,
    `Password` = `pPassword`,
    `PuntosDisponibles` = `pPuntosDisponibles`,
    `FkPersona` = `pFkPersona`,
    `IsAdmin` = `pIsAdmin`
  WHERE 
    (`IdUsuario` = `pIdUsuario`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spUpdateUsuarioPoints`;

DELIMITER  |
CREATE PROCEDURE `spUpdateUsuarioPoints` (
  IN `pIdUsuario` int,
  IN `pPuntosDisponibles` int
)
BEGIN
  UPDATE `Usuario` SET
    `PuntosDisponibles` = `pPuntosDisponibles`
  WHERE
    (`IdUsuario` = `pIdUsuario`);
END|
DELIMITER ;
   
DROP PROCEDURE IF EXISTS `spInsertUsuario`;

DELIMITER  |
CREATE PROCEDURE `spInsertUsuario` (
  IN `pUserName` varchar(50),
  IN `pEmail` varchar(50),
  IN `pPassword` varchar(150),
  IN `pPuntosDisponibles` int,
  IN `pFkPersona` int,
  IN `pIsAdmin` bit
)
BEGIN
  INSERT INTO `Usuario`
  (
    `UserName`,
    `Email`,
    `Password`,
    `PuntosDisponibles`,
    `FkPersona`,
    `IsAdmin`
  )
  VALUES 
  (
    `pUserName`,
    `pEmail`,
    `pPassword`,
    `pPuntosDisponibles`,
    `pFkPersona`,
    `pIsAdmin`
  );
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spDeleteUsuario`;

DELIMITER  |
CREATE PROCEDURE `spDeleteUsuario` (
   IN 
  `pIdUsuario`
   int
)
BEGIN
  DELETE FROM `Usuario`
  WHERE     
    (`IdUsuario` = `pIdUsuario`);
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spInsertarPersonaUsuario`;

DELIMITER  |
CREATE PROCEDURE spInsertarPersonaUsuario(
	IN pnombre 		VARCHAR(50),
	IN papellido 	VARCHAR(50),
	IN pnacimiento 	DATE,
	IN pfoto 		BLOB,
	IN pdistrito 	INT,
	IN pdireccion 	VARCHAR(200),

	IN pusername 	VARCHAR(50),
	IN pemail 		VARCHAR(50),
	IN ppassword 	VARCHAR(150)
	)
BEGIN
   DECLARE id_persona INT;
   START TRANSACTION;
   call spInsertPersona(pnombre,papellido,pnacimiento,pfoto,pdistrito,pdireccion);
   SELECT MAX(IdPersona) INTO id_persona FROM Persona;
   call spInsertUsuario(pusername,pemail,ppassword,0,id_persona,0);
   COMMIT;
END|
DELIMITER ;

DROP FUNCTION IF EXISTS `spIsUsernameFree`;
DELIMITER  |
CREATE FUNCTION spIsUsernameFree (pusername VARCHAR(150))
RETURNS  bit READS SQL DATA
BEGIN
   DECLARE username_count INT;
   select count(UserName) into username_count from Usuario where UserName=pusername;
   IF (username_count > 0) THEN
      RETURN 0;
   ELSE
      RETURN 1;
   end if ;
END|
DELIMITER ;

DROP PROCEDURE IF EXISTS `spComercioProductsWithoutImage`;
DELIMITER  |
CREATE PROCEDURE `spComercioProductsWithoutImage` (IN pIdComercio INT)
BEGIN
   DECLARE username_count INT;
   select IdProducto,
   Descripcion,
   FkComericio,
   Valor
   from Producto p join ProductoxComercio pc on p.IdProducto=pc.FkProducto where FkComericio=pIdComercio;
END|
DELIMITER ;


