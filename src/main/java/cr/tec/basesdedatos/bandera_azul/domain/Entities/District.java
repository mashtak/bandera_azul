package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class District {
    private int id;
    private String name;
    private int id_canton;

    public District(int id, String name, int id_canton) {
        this.id = id;
        this.name = name;
        this.id_canton = id_canton;
    }

    public District() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_canton() {
        return id_canton;
    }

    public void setId_canton(int id_canton) {
        this.id_canton = id_canton;
    }
}
