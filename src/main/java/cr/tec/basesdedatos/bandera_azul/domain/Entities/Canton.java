package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class Canton {
    private int id;
    private String name;
    private int id_provincia;

    public Canton(int id, String name, int id_provincia) {
        this.id = id;
        this.name = name;
        this.id_provincia = id_provincia;
    }

    public Canton() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_provincia() {
        return id_provincia;
    }

    public void setId_provincia(int id_provincia) {
        this.id_provincia = id_provincia;
    }
}
