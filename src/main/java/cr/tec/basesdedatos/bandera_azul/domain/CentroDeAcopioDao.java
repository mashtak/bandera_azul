package cr.tec.basesdedatos.bandera_azul.domain;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.*;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class CentroDeAcopioDao {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void registerCentroAcopio(CentroDeAcopio centro){
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withProcedureName("spInsertCentroAcopio")
                .withoutProcedureColumnMetaDataAccess();
        //   IN `pNombre` varchar(50),
        //  IN `pLongitud` float,
        //  IN `pLatitud` float,
        //  IN `pFkAdministrador` int
        jdbcCall.declareParameters(
                new SqlParameter("pNombre", Types.VARCHAR),
                new SqlParameter("pLongitud", Types.FLOAT),
                new SqlParameter("pLatitud", Types.FLOAT),
                new SqlParameter("pFkAdministrador", Types.INTEGER)
        );
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pNombre",centro.getName())
                .addValue("pLongitud",centro.getLongitud())
                .addValue("pLatitud",centro.getLatitud())
                .addValue("pFkAdministrador",centro.getId_administrator()
                );
        jdbcCall.execute(in);
        return;
    }
    private class CentroAcopioRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            CentroDeAcopio centroDeAcopio = new CentroDeAcopio(rs.getInt("IdCentroAcopio"),
                    rs.getString("Nombre"),rs.getDouble("Latitud"),
                    rs.getDouble("Longitud"),rs.getInt("FkAdministrador"));
            return centroDeAcopio;
        }
    }

    public List<CentroDeAcopio> getAllCentros(){
        String sql = "call spSelectCentroAcopio;";
        List<CentroDeAcopio> resultList = jdbcTemplate.query(sql,new CentroAcopioRowMapper());
        return  resultList;
    }
//    spSelectCentroAcopioByUserAdmin
    public List<CentroDeAcopio> getAllCentrosByUserAdmin(Usuario usuario){
        String sql = "call spSelectCentroAcopioByUserAdmin("+usuario.getId()+");";
        List<CentroDeAcopio> resultList = jdbcTemplate.query(sql,new CentroAcopioRowMapper());
        return  resultList;
    }

    public CentroDeAcopio getCentroAcopio(int id){
        String sql = "call spFindCentroAcopio("+id+");";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        Map<String, Object> centroMap = list.get(0);
        return new CentroDeAcopio(id,(String)centroMap.get("Nombre"),
                ((Float)centroMap.get("Latitud")).doubleValue(),((Float)centroMap.get("Longitud")).doubleValue(),
                (int)centroMap.get("FkAdministrador"));
    }

    private class MaterialRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Material material = new Material(rs.getInt("IdMaterial"),
                    rs.getString("Descripcion"),
                    rs.getInt("Valor"),
                    rs.getInt("FkTipoUnidad")
            );
            return material;
        }
    }
    public List<Material> getMaterials(){
        String sql = "call spSelectMaterial;";
        List<Material> resultList = jdbcTemplate.query(sql,new MaterialRowMapper());
        return  resultList;
    }

    private class UnitTypeRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Pair<Integer,String> pair = new Pair<>(
                    rs.getInt("IdTipoUnidad"),
                    rs.getString("Descripcion")
            );
            return pair;
        }
    }
    public List<Pair<Integer,String>> getUnitTypes(){
        String sql = "call spSelectTipoUnidad;";
        List<Pair<Integer,String>> resultList = jdbcTemplate.query(sql,new UnitTypeRowMapper());
        return  resultList;
    }

    public void registerRecyclation(AcumulatedPoints acumulatedPoints){
//        spInsertPuntosAcumulados
//        spInsertPuntosAcumulados
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withProcedureName("spInsertPuntosAcumulados")
                .withoutProcedureColumnMetaDataAccess();
//        IN `pFkUsuario` int,
//        IN `pFkMaterial` int,
//        IN `pFkCentroAcopio` int,
//        IN `pCantidad` int,
//        IN `pFecha` datetime
        jdbcCall.declareParameters(
                new SqlParameter("pFkUsuario", Types.INTEGER),
                new SqlParameter("pFkMaterial", Types.INTEGER),
                new SqlParameter("pFkCentroAcopio", Types.INTEGER),
                new SqlParameter("pCantidad", Types.INTEGER),
                new SqlParameter("pFecha", Types.DATE)
        );
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pFkUsuario",acumulatedPoints.getUsuario())
                .addValue("pFkMaterial",acumulatedPoints.getId_material())
                .addValue("pFkCentroAcopio",acumulatedPoints.getId_centro())
                .addValue("pCantidad",acumulatedPoints.getQuantity())
                .addValue("pFecha",new Date()
                );
        jdbcCall.execute(in);
        return;
    }
    public void adjustPoints(Usuario usuario,int newPoints){
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withProcedureName("spInsertPuntosAcumulados")
                .withoutProcedureColumnMetaDataAccess();

    }
}
