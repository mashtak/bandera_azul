package cr.tec.basesdedatos.bandera_azul.domain;


import cr.tec.basesdedatos.bandera_azul.domain.Entities.Comercio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Persona;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Product;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

@Repository
public class ComercioDao {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private class ComercioTypeRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Pair<Integer,String> pair = new Pair<>(rs.getInt("IdTipoComercio"),
                    rs.getString("Nombre"));
            return pair;
        }
    }

    public List<Pair<Integer,String>> getComercioTypes(){
        String sql = "call spSelectTipoComercio;";
        List<Pair<Integer,String>> resultList = jdbcTemplate.query(sql,new ComercioTypeRowMapper());
        return  resultList;
    }

    private class ComercioRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Comercio comercio = new Comercio(rs.getInt("IdComercioAutorizado"),
                    rs.getString("Nombre"),rs.getInt("FkTipoComercio"),
                    rs.getInt("FkAdministrador"));
            return comercio;
        }
    }
    public List<Comercio> getAllComercios(){
        String sql = "call spSelectComercioAutorizado;";
        List<Comercio> resultList = jdbcTemplate.query(sql,new ComercioRowMapper());
        return  resultList;
    }

    public void registerComercio(Comercio comercio){
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withProcedureName("spInsertPoductoConComercio")
                .withoutProcedureColumnMetaDataAccess();
//        IN `pNombre` varchar(50),
//                IN `pFkTipoComercio` int,
//        IN `pFkAdministrador`
        jdbcCall.declareParameters(
                new SqlParameter("pNombre", Types.VARCHAR),
                new SqlParameter("pFkTipoComercio", Types.INTEGER),
                new SqlParameter("pFkAdministrador", Types.INTEGER)
        );
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pNombre",comercio.getName())
                .addValue("pFkTipoComercio",comercio.getId_tipo())
                .addValue("pFkAdministrador",comercio.getId_administrator()
                );
        jdbcCall.execute(in);
        return;
    }

    public void registerProduct(Product product){
        System.out.println(product);
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withProcedureName("spInsertPoductoConComercio")
                .withoutProcedureColumnMetaDataAccess();
        //  IN `pDescripcion` varchar(50),
        //  IN `pFoto` blob,
        //  IN `pFkComericio` int,
        //  IN `pValor` int
        jdbcCall.declareParameters(
                new SqlParameter("pDescripcion", Types.VARCHAR),
                new SqlParameter("pFoto", Types.BLOB),
                new SqlParameter("pFkComericio", Types.INTEGER),
                new SqlParameter("pValor", Types.INTEGER)
        );
        SqlParameterSource in = null
                ;
        try {
            in = new MapSqlParameterSource()
                    .addValue("pDescripcion",product.getName())
                    .addValue("pFoto",new SqlLobValue(product.getPhoto().getBytes()))
                    .addValue("pFkComericio",product.getId_comercio())
                    .addValue("pValor",product.getPrice());
        } catch (IOException e) {
            e.printStackTrace();
        }
        jdbcCall.execute(in);

        return;
    }

    private class ProductWithoutImageRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Product product = new Product(rs.getInt("IdProducto"),
                    rs.getString("Descripcion"),
                    rs.getInt("Valor"),
                    rs.getInt("FkComericio")
            );
            return product;
        }
    }

    public List<Product> getComercioProductsWithoutImage(int idComercio){
        String sql = "call spComercioProductsWithoutImage("+idComercio+");";
        List<Product> resultList = jdbcTemplate.query(sql,new ProductWithoutImageRowMapper());
        return resultList;

    }
    public List<Comercio> getAllComerciosByUserAdmin(Usuario usuario){
        String sql = "call spSelectComercioAutorizadoByUserAdmin("+usuario.getId()+");";
        List<Comercio> resultList = jdbcTemplate.query(sql,new ComercioRowMapper());
        return  resultList;
    }
//    call spSelectComercioAutorizadoByUserAdmin(3);

    public int getComercioTotalPoints(int id){
        return 354;
    }
}
