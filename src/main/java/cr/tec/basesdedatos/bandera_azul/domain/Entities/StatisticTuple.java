package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class StatisticTuple {
    String name;
    int value;
    double percentage;

    public StatisticTuple(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public StatisticTuple() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
}
