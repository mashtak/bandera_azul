package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class Provincia {
    private int id;
    private String name;
    private int id_pais;

    public Provincia(int id, String name, int id_pais) {
        this.id = id;
        this.name = name;
        this.id_pais = id_pais;
    }

    public Provincia() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_pais() {
        return id_pais;
    }

    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }
}
