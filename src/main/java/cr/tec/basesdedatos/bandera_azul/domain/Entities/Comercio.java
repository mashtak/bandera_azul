package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class Comercio {
    int id;
    String name;
    int id_tipo;
    int id_administrator;

    public Comercio(int id, String name, int tipo, int id_administrator) {
        this.id = id;
        this.name = name;
        this.id_tipo = tipo;
        this.id_administrator = id_administrator;
    }

    public Comercio() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public int getId_administrator() {
        return id_administrator;
    }

    public void setId_administrator(int id_administrator) {
        this.id_administrator = id_administrator;
    }

    @Override
    public String toString() {
        return "Comercio{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", id_tipo='" + id_tipo + '\'' +
                ", id_administrator=" + id_administrator +
                '}';
    }
}
