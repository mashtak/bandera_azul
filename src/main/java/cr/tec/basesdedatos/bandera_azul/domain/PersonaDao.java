package cr.tec.basesdedatos.bandera_azul.domain;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.Persona;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

/**
 *
 * @author Jan Masek
 * @date 10.5.2017
 */
@Repository
public class PersonaDao {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private class PersonaRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Persona persona = new Persona(rs.getInt("IdPersona"),
                    rs.getString("Nombre"),rs.getString("Apellido"),
                    rs.getDate("FechaNacimiento"),rs.getInt("FkDistrito"),
                    rs.getString("Direccion"));
            return persona;
        }
    }
    public List<Persona> getAllPersonasWithoutImage(){
        String sql = "call spSelectPersonaWithoutImage;";
        List<Persona> resultList = jdbcTemplate.query(sql,new PersonaRowMapper());
        return  resultList;
    }
    public Persona getPersona(int id){
        //procedure GET_PERSONA( pid IN NUMBER,pnombre OUT VARCHAR2,
        //                      papellido OUT VARCHAR2,pfecha OUT DATE)
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withCatalogName("consultar2").withProcedureName("GET_PERSONA")
                .withoutProcedureColumnMetaDataAccess();
        jdbcCall.declareParameters(
                new SqlParameter("pid", Types.NUMERIC),
                new SqlOutParameter("pnombre", Types.VARCHAR),
                new SqlOutParameter("papellido", Types.VARCHAR),
                new SqlOutParameter("pfecha", Types.DATE)
        );
        SqlParameterSource in = new MapSqlParameterSource().addValue("pid", id);
        Map out = jdbcCall.execute(in);
        Persona persona = new Persona();
        persona.setCedula(id);
        persona.setNombre((String)out.get("pnombre"));
        persona.setApellido((String)out.get("papellido"));
        persona.setFechaDeNacimiento((Date)out.get("pfecha"));
        return persona;
    }

    @Transactional
    public byte[] getUserImage(int id){
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withCatalogName("consultar2").withProcedureName("GET_PERSONA_IMAGE")
                .withoutProcedureColumnMetaDataAccess();
        jdbcCall.declareParameters(
                new SqlParameter("pid", Types.NUMERIC),
                new SqlOutParameter("fotografia", Types.BLOB)
        );
        SqlParameterSource in = new MapSqlParameterSource().addValue("pid", id);
        Map out = jdbcCall.execute(in);
        Blob blob = (Blob) out.get("fotografia");
        byte[] foto =null;
        try {
            foto = blob.getBytes(1, (int) blob.length());
            blob.free();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return foto;
    }
}
