package cr.tec.basesdedatos.bandera_azul.domain.Entities;

import java.util.Date;

public class AcumulatedPoints {
    int id;
    int usuario;
    int id_material;
    int id_centro;
    int quantity;
    Date date;

    public AcumulatedPoints() {
    }

    public AcumulatedPoints(int id, int usuario, int id_material, int id_centro, int quantity, Date date) {
        this.id = id;
        this.usuario = usuario;
        this.id_material = id_material;
        this.id_centro = id_centro;
        this.quantity = quantity;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public int getId_material() {
        return id_material;
    }

    public void setId_material(int id_material) {
        this.id_material = id_material;
    }

    public int getId_centro() {
        return id_centro;
    }

    public void setId_centro(int id_centro) {
        this.id_centro = id_centro;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
