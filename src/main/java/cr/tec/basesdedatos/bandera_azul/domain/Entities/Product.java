package cr.tec.basesdedatos.bandera_azul.domain.Entities;

import org.springframework.web.multipart.MultipartFile;

public class Product {
    private int id;
    private String name;
    private int price;
    private int id_comercio;
    private MultipartFile photo;

    public Product() {
    }

    public Product(int id, String name, int price, int id_comercio) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.id_comercio = id_comercio;
    }

    public int getId_comercio() {
        return id_comercio;
    }

    public void setId_comercio(int id_comercio) {
        this.id_comercio = id_comercio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", id_comercio=" + id_comercio +
                '}';
    }
}
