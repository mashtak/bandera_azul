package cr.tec.basesdedatos.bandera_azul.domain.Entities;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 *
 * @author Jan Masek
 * @date 17.4.2017
 */
public class Persona {
    private int cedula;
    private String nombre;
    private String apellido;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaDeNacimiento;
    private int id_district;
    private String direccion;
    private MultipartFile fotografia;

    private boolean administrator;

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }
    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(Date fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public MultipartFile  getFotografia() {
        return fotografia;
    }

    public void setFotografia(MultipartFile  fotografia) {
        this.fotografia = fotografia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getId_district() {
        return id_district;
    }

    public void setId_district(int id_district) {
        this.id_district = id_district;
    }

    public Persona(int cedula, String nombre, String apellido, Date fechaDeNacimiento, int id_district,
                   String direccion){
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.id_district = id_district;
        this.direccion =direccion;
    }

    public Persona() {
    }
    public void copy(Persona persona){
        this.cedula = persona.cedula;
        this.nombre = persona.nombre;
        this.apellido = persona.apellido;
        this.fechaDeNacimiento = persona.fechaDeNacimiento;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "cedula=" + cedula +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", fechaDeNacimiento=" + fechaDeNacimiento +
                ", administrator=" + administrator +
                '}';
    }
}
