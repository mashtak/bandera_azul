package cr.tec.basesdedatos.bandera_azul.domain.Entities;

/**
 *
 * @author Jan Masek
 * @date 17.4.2017
 */
public class Usuario {
    private int id;
    private String username;
    private String password;
    private String email;
    private int idPersona;
    private int points;
    private boolean isSuperAdmin;


    public Usuario(int id, String username, String password, String email, int idPersona, int points, boolean isSuperAdmin) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.idPersona = idPersona;
        this.points = points;
        this.isSuperAdmin = isSuperAdmin;
    }

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public boolean isSuperAdmin() {
        return isSuperAdmin;
    }

    public void setSuperAdmin(boolean superAdmin) {
        isSuperAdmin = superAdmin;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", idPersona=" + idPersona +
                '}';
    }
}
