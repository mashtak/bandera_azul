package cr.tec.basesdedatos.bandera_azul.domain.Entities;

import java.util.Date;

public class RedeemedPoints {
    int id;
    int usuario;
    int id_producto;
    int id_comercio;
    int quantity;
    Date date;

    public RedeemedPoints() {
    }

    public RedeemedPoints(int id, int usuario, int id_producto, int id_comercio, int quantity, Date date) {
        this.id = id;
        this.usuario = usuario;
        this.id_producto = id_producto;
        this.id_comercio = id_comercio;
        this.quantity = quantity;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getId_comercio() {
        return id_comercio;
    }

    public void setId_comercio(int id_comercio) {
        this.id_comercio = id_comercio;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
