package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class CentroDeAcopio {
    int id;
    String name;
    double longitud;
    double latitud;
    int id_administrator;

    public CentroDeAcopio() {
    }

    public CentroDeAcopio(int id, String name,  double latitud,double longitud, int id_administrator) {
        this.id = id;
        this.name = name;
        this.longitud = longitud;
        this.latitud = latitud;
        this.id_administrator = id_administrator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public int getId_administrator() {
        return id_administrator;
    }

    public void setId_administrator(int id_administrator) {
        this.id_administrator = id_administrator;
    }

    @Override
    public String toString() {
        return "CentroDeAcopio{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", longitud=" + longitud +
                ", latitud=" + latitud +
                ", id_administrator=" + id_administrator +
                '}';
    }
}
