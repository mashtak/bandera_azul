package cr.tec.basesdedatos.bandera_azul.domain;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.StatisticTuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.LinkedList;
import java.util.List;

@Repository
public class StatisticDao {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<StatisticTuple> getGeneralAgeStatistics(){
        //todo
        List<StatisticTuple> statistic = new LinkedList<>();
        statistic.add(new StatisticTuple("0-18",15));
        statistic.add(new StatisticTuple("19-30",35));
        statistic.add(new StatisticTuple("31-45",20));
        return statistic;
    }

    public List<StatisticTuple> getGeneralHortalizaComestibleStatistic(){
        //todo
        List<StatisticTuple> statistic = new LinkedList<>();
        statistic.add(new StatisticTuple("Fruto",5));
        statistic.add(new StatisticTuple("Raíz",8));
        statistic.add(new StatisticTuple("Hoja",16));
        return statistic;
    }

    public List<StatisticTuple> getGeneralHortalizaColorStatistic(){
        //todo
        List<StatisticTuple> statistic = new LinkedList<>();
        statistic.add(new StatisticTuple("Rojo",3));
        statistic.add(new StatisticTuple("Azul",15));
        statistic.add(new StatisticTuple("Amarillo",24));
        return statistic;
    }

    public List<StatisticTuple> getGeneralArbolPeligroStatistic(){
        //todo
        List<StatisticTuple> statistic = new LinkedList<>();
        statistic.add(new StatisticTuple("En peligro de extinción",36));
        statistic.add(new StatisticTuple("Seguro",15));
        return statistic;
    }

    public List<StatisticTuple> getGeneralTrequeAnoStatistic(){
        //todo
        List<StatisticTuple> statistic = new LinkedList<>();
        statistic.add(new StatisticTuple("2018",36));
        statistic.add(new StatisticTuple("2017",26));
        statistic.add(new StatisticTuple("2016",48));
        statistic.add(new StatisticTuple("2015",32));
        statistic.add(new StatisticTuple("201č",14));
        return statistic;
    }

    public List<StatisticTuple> getGeneralVentaAnoStatistic(){
        //todo
        List<StatisticTuple> statistic = new LinkedList<>();
        statistic.add(new StatisticTuple("2018",8));
        statistic.add(new StatisticTuple("2017",4));
        statistic.add(new StatisticTuple("2016",100));
        statistic.add(new StatisticTuple("2015",100));
        statistic.add(new StatisticTuple("2014",5));
        return statistic;
    }
}
