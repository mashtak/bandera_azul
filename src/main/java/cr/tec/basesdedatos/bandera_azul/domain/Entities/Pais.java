package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class Pais {
    private int id;
    private String name;

    public Pais(int id,String name) {
        this.name = name;
        this.id = id;
    }

    public Pais() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
