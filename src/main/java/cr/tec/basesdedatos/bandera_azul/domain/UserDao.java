package cr.tec.basesdedatos.bandera_azul.domain;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//http://www.baeldung.com/spring-security-registration-password-encoding-bcrypt
@Repository
public class UserDao {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Usuario findUser(String username){
        String sql = "call spFindUsuario('"+username+"');";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        Map<String, Object> usuarioMap = list.get(0);
        return new Usuario((int)usuarioMap.get("IdUsuario"),username,(String)usuarioMap.get("Password"),
                (String)usuarioMap.get("Email"),(int)usuarioMap.get("FkPersona"),(int)usuarioMap.get("PuntosDisponibles"),
                (boolean)usuarioMap.get("IsAdmin"));
    }

    public boolean isUsernameFree(String username){
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withFunctionName("spIsUsernameFree");
        jdbcCall.declareParameters(
                new SqlParameter("pusername", Types.VARCHAR));
        SqlParameterSource in = new MapSqlParameterSource().addValue("pusername", username);
        System.out.println(username);
        Boolean return_value = jdbcCall.executeFunction(Boolean.class, in);
        System.out.println(return_value);
        return return_value;
    }

    public boolean registerUsario(Usuario usuario, Persona persona) throws IOException {
//            spInsertarPersonaUsuario(
//	IN pnombre 		VARCHAR(50),
//	IN papellido 	VARCHAR(50),
//	IN pnacimiento 	DATE,
//	IN pfoto 		BLOB,
//	IN pdistrito 	INT,
//	IN pdireccion 	VARCHAR(200),
//
//	IN pusername 	VARCHAR(50),
//	IN pemail 		VARCHAR(50),
//	IN ppassword 	VARCHAR(150)
//	)
        SimpleJdbcCall jdbcCall = new
                SimpleJdbcCall(dataSource).withProcedureName("spInsertarPersonaUsuario")
                .withoutProcedureColumnMetaDataAccess();
        jdbcCall.declareParameters(
                new SqlParameter("pnombre", Types.VARCHAR),
                new SqlParameter("pappelido", Types.VARCHAR),
                new SqlParameter("pnacimiento", Types.DATE),
                new SqlParameter("pfoto", Types.BLOB),
                new SqlParameter("pdistrito", Types.INTEGER),
                new SqlParameter("pdireccion", Types.VARCHAR),
                new SqlParameter("pusername", Types.VARCHAR),
                new SqlParameter("pemail", Types.VARCHAR),
                new SqlParameter("ppassword", Types.VARCHAR)
        );
        SqlParameterSource in = new MapSqlParameterSource()
                                        .addValue("pnombre",persona.getNombre())
                                        .addValue("pappelido",persona.getApellido())
                                        .addValue("pnacimiento",persona.getFechaDeNacimiento())
                                        .addValue("pdistrito",persona.getId_district())
                                        .addValue("pdireccion",persona.getDireccion())
                                        .addValue("pusername", usuario.getUsername())
                                        .addValue("pemail",usuario.getEmail())
                                        .addValue("ppassword",usuario.getPassword())
                                        .addValue("pfoto",new SqlLobValue(persona.getFotografia().getBytes()));
        jdbcCall.execute(in);

        return true;
    }

    private class UserRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario usuario = new Usuario(rs.getInt("IdUsuario"),
                    rs.getString("UserName"),rs.getString("Password"),
                    rs.getString("Email"),rs.getInt("FkPersona"),
                    rs.getInt("PuntosDisponibles"),rs.getBoolean("IsAdmin"));
            return usuario;
        }
    }
    public List<Usuario> getAllUsers(){
        String sql = "call spSelectUsuario;";
        List<Usuario> resultList = jdbcTemplate.query(sql,new UserRowMapper());
        return  resultList;
    }





//    public List<Huerta> getAllUserAdminHuertas(Usuario usuario){
//        List<Huerta> huertaList = new LinkedList<>();
//        huertaList.add(new Huerta(1,"Huerta 1",-84.071660,9.914847));
//        huertaList.add(new Huerta(2,"Huerta 2",-84.071660,10.914847));
//        return huertaList;
//    }




}
