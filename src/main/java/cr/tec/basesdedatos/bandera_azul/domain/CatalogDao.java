package cr.tec.basesdedatos.bandera_azul.domain;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class CatalogDao {

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private class PaisRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Pais pais = new Pais(rs.getInt("IdPais"),
                    rs.getString("Nombre"));
            return pais;
        }
    }
    public Map<Integer,Pais> getPaises(){
        String sql = "call spSelectPais;";
        List<Pais> resultList = jdbcTemplate.query(sql,new PaisRowMapper());
        Map<Integer,Pais> map =
                resultList.stream().collect(Collectors.toMap(Pais::getId, item -> item));
        return map;
    }

    private class ProvinciaRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Provincia provincia = new Provincia(rs.getInt("IdProvincia"),
                    rs.getString("Nombre"),rs.getInt("FkPais"));
            return provincia;
        }
    }
    public Map<Integer,Provincia> getProvincias(){
        String sql = "call spSelectProvincia;";
        List<Provincia> resultList = jdbcTemplate.query(sql,new ProvinciaRowMapper());
        Map<Integer,Provincia> map =
                resultList.stream().collect(Collectors.toMap(Provincia::getId, item -> item));
        return map;
    }

    private class CantonRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            Canton canton = new Canton(rs.getInt("IdCanton"),
                    rs.getString("Nombre"),rs.getInt("FkProvincia"));
            return canton;
        }
    }
    public Map<Integer,Canton> getCantons(){
        String sql = "call spSelectCanton;";
        List<Canton> resultList = jdbcTemplate.query(sql,new CantonRowMapper());
        Map<Integer,Canton> map =
                resultList.stream().collect(Collectors.toMap(Canton::getId, item -> item));
        return map;
    }

    private class DistrictRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            District district = new District(rs.getInt("IdDistrito"),
                    rs.getString("Nombre"),rs.getInt("FkCanton"));
            return district;
        }
    }
    public Map<Integer,District> getDistricts(){
        String sql = "call spSelectDistrito;";
        List<District> resultList = jdbcTemplate.query(sql,new DistrictRowMapper());
        Map<Integer,District> map =
                resultList.stream().collect(Collectors.toMap(District::getId, item -> item));
        return map;
    }


}
