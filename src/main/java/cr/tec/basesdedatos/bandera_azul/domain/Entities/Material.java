package cr.tec.basesdedatos.bandera_azul.domain.Entities;

public class Material {
    int id;
    String description;
    int value;
    int id_unit;

    public Material() {
    }

    public Material(int id, String description, int value, int id_unit) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.id_unit = id_unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getId_unit() {
        return id_unit;
    }

    public void setId_unit(int id_unit) {
        this.id_unit = id_unit;
    }

}
