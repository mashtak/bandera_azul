package cr.tec.basesdedatos.bandera_azul.service;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.StatisticTuple;
import cr.tec.basesdedatos.bandera_azul.domain.StatisticDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticService {
    @Autowired
    StatisticDao statisticDao;

    public List<StatisticTuple> getGeneralAgeStatistics(){
        return computePercentage(statisticDao.getGeneralAgeStatistics());
    }
    public List<StatisticTuple> getGeneralArbolPeligroStatistic(){
        return computePercentage(statisticDao.getGeneralArbolPeligroStatistic());
    }
    public List<StatisticTuple> getGeneralHortalizaColorStatistic(){
        return computePercentage(statisticDao.getGeneralHortalizaColorStatistic());

    }
    public List<StatisticTuple> getGeneralHortalizaComestibleStatistic(){
        return computePercentage(statisticDao.getGeneralHortalizaComestibleStatistic());
    }
    public List<StatisticTuple> getGeneralVentaAnoStatistic(){
        return computePercentage(statisticDao.getGeneralVentaAnoStatistic());

    }
    public List<StatisticTuple> getGeneralTrequeAnoStatistic(){
        return computePercentage(statisticDao.getGeneralTrequeAnoStatistic());
    }


    private List<StatisticTuple> computePercentage(List<StatisticTuple> statistics){
        long sum=0;
        for (StatisticTuple data:statistics) {
            sum += data.getValue();
        }
        for (StatisticTuple data:statistics) {
            double percentage = Math.round(((double)data.getValue())/sum*100*100);
            percentage /= 100.0;
            data.setPercentage(percentage);
        }
        return statistics;
    }
}
