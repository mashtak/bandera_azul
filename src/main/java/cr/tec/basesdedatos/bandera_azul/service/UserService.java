package cr.tec.basesdedatos.bandera_azul.service;

import cr.tec.basesdedatos.bandera_azul.domain.CentroDeAcopioDao;
import cr.tec.basesdedatos.bandera_azul.domain.ComercioDao;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.*;
import cr.tec.basesdedatos.bandera_azul.domain.UserDao;
import cr.tec.basesdedatos.bandera_azul.service.entities.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Jan Masek
 * @date 3.4.2017
 */
@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserDao userDao;
    @Autowired
    ComercioDao comercioDao;
    @Autowired
    CentroDeAcopioDao centroDeAcopioDao;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public boolean registerUser(Usuario usuario, Persona persona) throws IOException {
//        check if the username is ok
//        if(!userDao.isUsernameFree(usuario.getUsername()))
//            throw new UsernameNotFoundException(usuario.getUsername());
        usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
        return userDao.registerUsario(usuario,persona);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = userDao.findUser(username);
        if(usuario == null) {
            throw new UsernameNotFoundException(username);
        }
        return createUser(usuario);
    }

    private LoginUser createUser(Usuario usuario){
        List<Comercio> comerciosAdmin = getComerciosAdmin(usuario);
        List<CentroDeAcopio> centrosAdmin = getCentrosAdmin(usuario);
        LoginUser loginUser = new LoginUser(
                usuario, true, true,
                true,true,
                getAuthorities(usuario.isSuperAdmin(),!centrosAdmin.isEmpty(),(!comerciosAdmin.isEmpty())),
                getComerciosAdmin(usuario),getCentrosAdmin(usuario));
        return loginUser;
    }

    private List<Comercio> getComerciosAdmin(Usuario usuario){
        return comercioDao.getAllComerciosByUserAdmin(usuario);
    }

    private List<CentroDeAcopio> getCentrosAdmin(Usuario usuario){
        return centroDeAcopioDao.getAllCentrosByUserAdmin(usuario);
    }


    //inspired by http://www.baeldung.com/role-and-privilege-for-spring-security-registration
    private Collection<? extends GrantedAuthority> getAuthorities(boolean isSuperAdmin,
                                                                  boolean isCentroAdmin,boolean isComercioAdmin) {
        return getGrantedAuthorities(getPrivileges(isSuperAdmin,isCentroAdmin,isComercioAdmin));
    }

    private List<String> getPrivileges(boolean isSuperAdmin,
                                       boolean isCentroAdmin,boolean isComercioAdmin) {
        List<String> privileges = new ArrayList<>();
        privileges.add("ROLE_USER");
        if(isSuperAdmin)
            privileges.add("ROLE_SUPERADMIN");
        if(isCentroAdmin)
            privileges.add("ROLE_CENTROADMIN");
        if(isComercioAdmin)
            privileges.add("ROLE_COMERCIOADMIN");
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }

    public Map<Integer,Usuario> getAllUsers(){
        List<Usuario> list = userDao.getAllUsers();
        Map<Integer,Usuario> map =
                list.stream().collect(Collectors.toMap(Usuario::getId, item -> item));
        System.out.println(list);
        return map;
    }

    public Map<Integer,Date> getUsersPasswordModifications(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Map<Integer,Date> map = new HashMap<>();
        try {
            map.put(1,sdf.parse("21/1/2018"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return map;
    }

    public Map<Integer,Date> getUsersPasswordModificationsFake(Map<Integer,Usuario> users) {
        Random random = new Random();
        Map<Integer,Date> map = new HashMap<>();
        for (Usuario user: users.values()) {
            map.put(user.getId(),new Date(new Date().getTime()-random.nextInt(90)*86400000));
        }
        return map;
    }

}

