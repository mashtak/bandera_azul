package cr.tec.basesdedatos.bandera_azul.service.entities;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.Comercio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.CentroDeAcopio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LoginUser extends org.springframework.security.core.userdetails.User {
    private List<Comercio> comerciosAdmin = new ArrayList<>();
    private List<CentroDeAcopio> centrosAdmin = new ArrayList<>();
    private Usuario usuario;


    public LoginUser(Usuario usuario, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired,
                     boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
                     List<Comercio> comerciosAdmin,List<CentroDeAcopio> centrosAdmin) {
        super(usuario.getUsername(), usuario.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities);
        this.usuario = usuario;
        this.comerciosAdmin = comerciosAdmin;
        this.centrosAdmin = centrosAdmin;
    }

    public List<Comercio> getComerciosAdmin() {
        return comerciosAdmin;
    }

    public List<CentroDeAcopio> getCentrosAdmin() {
        return centrosAdmin;
    }

    public int getPersonaId(){
        return usuario.getIdPersona();
    }

    public String getUsername(){
        return usuario.getUsername();
    }

    public int getUserId(){
        return usuario.getId();
    }
}
