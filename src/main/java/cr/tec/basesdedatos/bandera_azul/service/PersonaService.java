package cr.tec.basesdedatos.bandera_azul.service;


import cr.tec.basesdedatos.bandera_azul.domain.Entities.*;
import cr.tec.basesdedatos.bandera_azul.domain.PersonaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Jan Masek
 * @date 10.5.2017
 */
@Service
public class PersonaService {
    @Autowired
    PersonaDao personaDao;

    public Map<Integer,Persona> getAllPersonasWithoutImage(){
        List<Persona> list = personaDao.getAllPersonasWithoutImage();
        Map<Integer,Persona> map =
                list.stream().collect(Collectors.toMap(Persona::getCedula, item -> item));
//        System.out.println(map);
        return map;
    }

    public byte[] getPersonImage(int id){
        return personaDao.getUserImage(id);
    }
    public Persona getPersona(int id){ return personaDao.getPersona(id);}


}
