package cr.tec.basesdedatos.bandera_azul.service;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.AcumulatedPoints;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.RedeemedPoints;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PointsService {
    public Map<Integer,List<AcumulatedPoints>> getAllAcumulatedPoints(){
        Map<Integer,List<AcumulatedPoints>> map = new HashMap<>();
        List<AcumulatedPoints> list = new LinkedList<AcumulatedPoints>();
        list.add(new AcumulatedPoints(1,1,1,1,20,new Date()));
        map.put(1,list);
        return map;

    }
    public Map<Integer,List<RedeemedPoints>> getAllRedeemedPoints(){
        Map<Integer,List<RedeemedPoints>> map = new HashMap<>();
        List<RedeemedPoints> list = new LinkedList<RedeemedPoints>();
        list.add(new RedeemedPoints(1,1,1,1,10,new Date()));
        map.put(1,list);
        return map;
    }

    private long daymilis = 86400000;
    private Random random = new Random();
    public Map<Integer,List<RedeemedPoints>> getAllRedeemedPointsFake(Map<Integer,Usuario> users){
        Map<Integer,List<RedeemedPoints>> map = new HashMap<>();
        for (Usuario user :users.values()) {
            List<RedeemedPoints> list = new LinkedList<>();
            list.add(new RedeemedPoints(1,user.getId(),1,1,random.nextInt(100),new Date(new Date().getTime()-2*daymilis)));
            map.put(user.getId(),list);
        }
        return map;
    }
    public Map<Integer,List<AcumulatedPoints>> getAllAcumulatedPointsFake(Map<Integer,Usuario> users){
        Map<Integer,List<AcumulatedPoints>> map = new HashMap<>();
        for (Usuario user :users.values()) {
            List<AcumulatedPoints> list = new LinkedList<>();
            list.add(new AcumulatedPoints(1,user.getId(),1,1,random.nextInt(100),new Date(new Date().getTime()-daymilis)));
            map.put(user.getId(),list);
        }
        return map;

    }
}
