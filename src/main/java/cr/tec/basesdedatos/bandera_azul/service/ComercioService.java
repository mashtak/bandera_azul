package cr.tec.basesdedatos.bandera_azul.service;

import cr.tec.basesdedatos.bandera_azul.domain.ComercioDao;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Comercio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Persona;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Product;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.PortUnreachableException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ComercioService {
    @Autowired
    ComercioDao comercioDao;

    public List<Comercio> getAllComercios(){
//        List<Comercio> comercios = new LinkedList<>();
//        comercios.add(new Comercio(1,"Comercio 1",1,1));
//        comercios.add(new Comercio(2,"Comercio 2",2,1));
        return comercioDao.getAllComercios();
    }

    public Map<Integer,String> getComerciosTypes(){
        List<Pair<Integer, String>> list = comercioDao.getComercioTypes();
        Map<Integer,String> map =
                list.stream().collect(Collectors.toMap(item -> item.getKey(), item -> item.getValue()));
        return map;
    }

    public Comercio getComercio(int id){
        return new Comercio(1,"Comercio 1",1,1);
    }

    public int getComercioTotalPoints(int id){
        return comercioDao.getComercioTotalPoints(id);
    }
    public List<Product> getComercioProductsWithoutImage(int id_comercio){
//        List<Product> list = new LinkedList<>();
//        list.add(new Product(1,"Zapatos",500,1));
//        list.add(new Product(2,"Camisa",600,1));
        return comercioDao.getComercioProductsWithoutImage(id_comercio);
    }

    public void registerComercio(Comercio comercio){
        comercioDao.registerComercio(comercio);
        System.out.println(comercio);
    }

    public void registerProduct(Product product){
        comercioDao.registerProduct(product);
    }
    public Product getProduct(int id){
        return new Product(1,"Zapatos",500,1);
    }
}
