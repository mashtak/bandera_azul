package cr.tec.basesdedatos.bandera_azul.service;

import cr.tec.basesdedatos.bandera_azul.domain.CatalogDao;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Canton;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.District;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Pais;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Provincia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class CatalogService {
    @Autowired
    CatalogDao catalogDao;

    public Map<Integer,Pais> getAllPaises(){
//        Map<Integer,Pais> map = new HashMap<>();
//        map.put(1,new Pais(1,"Costa Rica"));
        return catalogDao.getPaises();
    }


    public Map<Integer,Provincia> getAllProvincias(){
//        Map<Integer,Provincia> map = new HashMap<>();
//        map.put(1,new Provincia(1,"Provincia 1",1));
        return catalogDao.getProvincias();
    }

    public Map<Integer,Canton> getAllCantons(){
//        Map<Integer,Canton> map = new HashMap<>();
//        map.put(1,new Canton(1,"Canton 1",1));
        return catalogDao.getCantons();
    }

    public Map<Integer,District> getAllDistricts(){
//        Map<Integer,District> map = new HashMap<>();
//        map.put(1, new District(1,"District 1",1));
//        map.put(2, new District(2,"2. District",1));
        return catalogDao.getDistricts();
    }

}
