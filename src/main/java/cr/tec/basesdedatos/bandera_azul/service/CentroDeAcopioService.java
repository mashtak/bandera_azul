package cr.tec.basesdedatos.bandera_azul.service;

import cr.tec.basesdedatos.bandera_azul.domain.CentroDeAcopioDao;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.AcumulatedPoints;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.CentroDeAcopio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Material;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import cr.tec.basesdedatos.bandera_azul.domain.UserDao;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CentroDeAcopioService {
    @Autowired
    CentroDeAcopioDao centroDeAcopioDao;

    @Autowired
    UserDao userDao;

    public List<CentroDeAcopio> getCentros(){
//        List<CentroDeAcopio> centros  = new LinkedList<>();
//        centros.add(new CentroDeAcopio(1,"Centro Acopio 1",
//                9.910048, -84.072948,1));
//        centros.add(new CentroDeAcopio(2,"Centro Acopio 2",
//                9.910048, -84.272948,1));
        return centroDeAcopioDao.getAllCentros();
    }

    public void registerCentroAcopio(CentroDeAcopio centroDeAcopio){
        centroDeAcopioDao.registerCentroAcopio(centroDeAcopio);
        System.out.println(centroDeAcopio);
    }

    public CentroDeAcopio getCentroAcopio(int id){
        return centroDeAcopioDao.getCentroAcopio(id);
    }
    public Map<Integer,Material> getMaterials(){
        Map<Integer,Material> map =
                centroDeAcopioDao.getMaterials().stream().collect(Collectors.toMap(Material::getId, item -> item));
        return map;
    }

    public Map<Integer,String> getUnitTypes(){
        List<Pair<Integer, String>> list = centroDeAcopioDao.getUnitTypes();
        Map<Integer,String> map =
                list.stream().collect(Collectors.toMap(item -> item.getKey(), item -> item.getValue()));
        return map;
    }

    public void registerRecyclation(AcumulatedPoints acumulatedPoints){
        centroDeAcopioDao.registerRecyclation(acumulatedPoints);
//        Usuario usuario = userDao.findUser().
//        centroDeAcopioDao.adjustPoints();

    }

}

