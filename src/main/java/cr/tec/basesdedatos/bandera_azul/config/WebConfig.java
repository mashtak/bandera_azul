package cr.tec.basesdedatos.bandera_azul.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author Jan Masek
 * @date 3.4.2017
 */
@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/css/**")
                .addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("/js/**")
                .addResourceLocations("classpath:/static/js/");
        registry.addResourceHandler("/*.png")
                .addResourceLocations("classpath:/static/images/");
        registry.addResourceHandler("/*.ico")
                .addResourceLocations("classpath:/static/images/");
        registry.addResourceHandler("/*.svg")
                .addResourceLocations("classpath:/static/images/");
        registry.addResourceHandler("/browserconfig.xml")
                .addResourceLocations("classpath:/static/images/browserconfig.xml");
        registry.addResourceHandler("/site.webmanifest")
                .addResourceLocations("classpath:/static/images/site.webmanifest");
        registry.addResourceHandler("/open-iconic/**")
                .addResourceLocations("classpath:/static/open-iconic/");

    }

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("index");
        registry.addViewController("/").setViewName("index");
//        registry.addViewController("/hello").setViewName("hello");
    }

}
