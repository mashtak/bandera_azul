package cr.tec.basesdedatos.bandera_azul.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 *
 * @author Jan Masek
 * @date 3.4.2017
 */
@PropertySource({"classpath:database.properties"})
@Configuration
@ComponentScan
public class AppConfig {

    @Value( "${database.url}" )
    private String databaseUrl;
    @Value( "${database.username}" )
    private String databaseUsername;
    @Value( "${database.password}" )
    private String databasePassword;


    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
//        ds.setDriverClassName(oracle.jdbc.driver.OracleDriver.class.getName());
        ds.setDriverClassName(com.mysql.jdbc.Driver.class.getName());
        ds.setUrl(databaseUrl);
        ds.setUsername(databaseUsername);
        ds.setPassword(databasePassword);
        return ds;
    }


}