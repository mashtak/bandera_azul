package cr.tec.basesdedatos.bandera_azul.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class CurrencyAdvice {
    @Value("${application.currency}")
    private String currency;

    @ModelAttribute("currency")
    public String getCurrentUser() {
        return currency;
    }
}
