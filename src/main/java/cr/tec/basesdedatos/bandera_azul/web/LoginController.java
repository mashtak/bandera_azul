package cr.tec.basesdedatos.bandera_azul.web;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.Persona;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import cr.tec.basesdedatos.bandera_azul.service.CatalogService;
import cr.tec.basesdedatos.bandera_azul.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
public class LoginController {
    @Autowired
    UserService userService;
    @Autowired
    CatalogService catalogService;

    // Login form
    @RequestMapping("/login")
    public String login() {
        return "userManagement/login.html";
    }

    // Login form with error
    @RequestMapping("/login-error.html")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login.html";
    }

    //Registration
    @GetMapping("/register")
    public ModelAndView registerUserForm(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("userManagement/registrarUsuario");
        modelAndView.addObject("paises", catalogService.getAllPaises());
        modelAndView.addObject("provincias",catalogService.getAllProvincias());
        modelAndView.addObject("cantons",catalogService.getAllCantons());
        modelAndView.addObject("districts",catalogService.getAllDistricts());
        modelAndView.addObject("usuario", new Usuario());
        modelAndView.addObject("persona", new Persona());
        return modelAndView;
    }

    @PostMapping("/users/register")
    public ModelAndView registerUserSubmit(@ModelAttribute Persona persona,
                                           @ModelAttribute Usuario usuario) throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        boolean success = userService.registerUser(usuario,persona);
        modelAndView.addObject("registration", success);
        modelAndView.setViewName("userManagement/login.html");
        return modelAndView;
    }

}
