package cr.tec.basesdedatos.bandera_azul.web;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import cr.tec.basesdedatos.bandera_azul.service.CatalogService;
import cr.tec.basesdedatos.bandera_azul.service.PersonaService;
import cr.tec.basesdedatos.bandera_azul.service.PointsService;
import cr.tec.basesdedatos.bandera_azul.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Map;

@Controller
public class UserController{
    @Autowired
    UserService userService;
    @Autowired
    PersonaService personaService;
    @Autowired
    CatalogService catalogService;
    @Autowired
    PointsService pointsService;

    @RequestMapping(value = "/users/{id}/image")
    @ResponseBody
    public byte[] getUserImage(@PathVariable("id") int id) throws IOException {
        byte[] imageContent = personaService.getPersonImage(id);
        return imageContent;
    }

    @RequestMapping(value = "/users/{id}")
    public ModelAndView user(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("userManagement/userDetail");
        modelAndView.addObject("persona",personaService.getPersona(id));
        return modelAndView;
    }

    @RequestMapping(value = "/superadmin/users")
    public ModelAndView getAllUsers(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("userManagement/showAll");
        modelAndView.addObject("usuarios",userService.getAllUsers());
        modelAndView.addObject("personas",personaService.getAllPersonasWithoutImage());
        modelAndView.addObject("paises", catalogService.getAllPaises());
        modelAndView.addObject("provincias",catalogService.getAllProvincias());
        modelAndView.addObject("cantons",catalogService.getAllCantons());
        modelAndView.addObject("districts",catalogService.getAllDistricts());
        return modelAndView;
    }

    @RequestMapping(value = "/superadmin/users/passwordsModifications")
    public ModelAndView getUsersPasswordModifications(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("personas",personaService.getAllPersonasWithoutImage());
        modelAndView.addObject("usuarios",userService.getAllUsers());
        modelAndView.addObject("passwordModifications",userService.getUsersPasswordModificationsFake(userService.getAllUsers()));
        modelAndView.setViewName("userManagement/passwordModifications");
        return modelAndView;
    }

    @RequestMapping(value = "/superadmin/users/points")
    public ModelAndView getUsersWihtPoints(){
        ModelAndView modelAndView = new ModelAndView();
        Map<Integer,Usuario> usuarios= userService.getAllUsers();
        modelAndView.addObject("redeemedPoints",pointsService.getAllRedeemedPointsFake(usuarios));
        modelAndView.addObject("acumulatedPoints",pointsService.getAllAcumulatedPointsFake(usuarios));
        modelAndView.addObject("personas",personaService.getAllPersonasWithoutImage());
        modelAndView.addObject("usuarios",usuarios);
//        modelAndView.addObject("usuarios",userService.getAllUsers());
        modelAndView.setViewName("userManagement/usersWithPoints");
        return modelAndView;
    }






}
