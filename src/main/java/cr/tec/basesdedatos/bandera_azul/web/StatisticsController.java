package cr.tec.basesdedatos.bandera_azul.web;

import cr.tec.basesdedatos.bandera_azul.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class StatisticsController {
    @Autowired
    StatisticService statisticService;


    @RequestMapping("/superadmin/statistics/userAge")
    public ModelAndView userAgeStatistic(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("statistic/userAge");
        modelAndView.addObject("ageStatistics",statisticService.getGeneralAgeStatistics());
       return modelAndView;
    }

    @RequestMapping("/superadmin/statistics/comercioType")
    public ModelAndView comercioTypeStatistic(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("statistic/comercioType");
        //TODO CALL service to obtain data
        return modelAndView;
    }

    @RequestMapping("/superadmin/statistics/productMonths")
    public ModelAndView productsMonthsStatistic(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("statistic/productMonths");
        //TODO CALL service to obtain data
        return modelAndView;
    }

    @RequestMapping("/superadmin/statistics/userPoints")
    public ModelAndView userPointsStatistic(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("statistic/userPoints");
        //TODO CALL service to obtain data
        return modelAndView;
    }



}
