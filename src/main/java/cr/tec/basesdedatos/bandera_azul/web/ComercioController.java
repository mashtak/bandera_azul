package cr.tec.basesdedatos.bandera_azul.web;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.CentroDeAcopio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Comercio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Product;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import cr.tec.basesdedatos.bandera_azul.service.ComercioService;
import cr.tec.basesdedatos.bandera_azul.service.PersonaService;
import cr.tec.basesdedatos.bandera_azul.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.*;

@Controller
public class ComercioController {
    @Autowired
    ComercioService comercioService;

    @Autowired
    PersonaService personaService;

    @Autowired
    UserService userService;


    @Value("${application.currency}")
    private String currency;


    @RequestMapping("/comercios/")
    public ModelAndView showAllComercios(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/showAll");
        modelAndView.addObject("comercios",comercioService.getAllComercios());
        modelAndView.addObject("comerciosTypes",comercioService.getComerciosTypes());
        modelAndView.addObject("usuarios",userService.getAllUsers());
        return modelAndView;
    }

    @RequestMapping("/comercios/{id}")
    public ModelAndView showComercio(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/show");
        modelAndView.addObject("comercio",comercioService.getComercio(id));
        modelAndView.addObject("comerciosTypes",comercioService.getComerciosTypes());
        modelAndView.addObject("products",comercioService.getComercioProductsWithoutImage(id));
        modelAndView.addObject("currency",currency);
        return modelAndView;
    }

    @RequestMapping("/comercios/{id_comercio}/products/{id_product}")
    public ModelAndView showProduct(@PathVariable("id_comercio") int id_comercio,
                                    @PathVariable("id_product") int id_product){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/showProduct");
        modelAndView.addObject("comercio",comercioService.getComercio(id_comercio));
        modelAndView.addObject("product",comercioService.getProduct(id_product));
        modelAndView.addObject("currency",currency);
        return modelAndView;
    }


    @GetMapping("/superadmin/comercios/register")
    public ModelAndView registerComercio(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/register");
        modelAndView.addObject("comercio", new Comercio());
        modelAndView.addObject("types",comercioService.getComerciosTypes());
        List<Usuario> usuarios = new LinkedList<>(userService.getAllUsers().values());
        usuarios.sort(Comparator.comparing(Usuario::getUsername));
        modelAndView.addObject("usuarios", usuarios);
        return modelAndView;
    }

    @PostMapping("/superadmin/comercios/register")
    public ModelAndView registerComercioSubmit(@ModelAttribute Comercio comercio) throws IOException {
        comercioService.registerComercio(comercio);
        return new ModelAndView("redirect:/comercios/");
    }

    @RequestMapping("/superadmin/comercios/topProducts")
    public ModelAndView topProductsGeneral(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/topProductsGeneral");
        //todo call service
        return modelAndView;
    }

    @RequestMapping("/superadmin/comercios/")
    public ModelAndView showComerciosSuperAdmin(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/adminShowAll");
        modelAndView.addObject("comercioTypes",comercioService.getComerciosTypes());
        modelAndView.addObject("commercios",comercioService.getAllComercios());
        modelAndView.addObject("users",userService.getAllUsers());
        modelAndView.addObject("personas",personaService.getAllPersonasWithoutImage());
        return modelAndView;
    }

    @GetMapping("/admin/comercios/{id}/products/register")
    public ModelAndView registerComercioProduct(@PathVariable("id") int id){
        //todo check if id admin
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/registerProduct");
        modelAndView.addObject("comercio", comercioService.getComercio(id));
        modelAndView.addObject("currency",currency);
        modelAndView.addObject("product",new Product());
        return modelAndView;
    }

    @PostMapping("/admin/comercios/{id}/products/register")
    public String registerComercioProductSubmit(@PathVariable("id") int id,
                                                @ModelAttribute Product product) throws IOException {
        //todo check if id admin
        //todo call service to register centro
        product.setId_comercio(id);
        comercioService.registerProduct(product);
        return "redirect:/admin/comercios/"+id+"/products" ;
    }

    @GetMapping("/admin/comercios/{id}/products")
    public ModelAndView getAllProductsAdmin(@PathVariable("id") int id){
        //todo check if id admin
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("comercio",comercioService.getComercio(id));
        modelAndView.addObject("products",comercioService.getComercioProductsWithoutImage(id));
        modelAndView.addObject("currency",currency);
        modelAndView.setViewName("comercioAfiliado/allProductsAdmin");
        return modelAndView;
    }

    @GetMapping("/admin/comercios/{id}/products/{id_producto}")
    public ModelAndView getProductAdmin(@PathVariable("id") int id,
                                            @PathVariable("id_producto") int id_producto){
        //todo check if id admin
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("comercio",comercioService.getComercio(id));
        modelAndView.addObject("product",comercioService.getProduct(id_producto));
        modelAndView.addObject("currency",currency);
        modelAndView.setViewName("comercioAfiliado/productAdmin");
        return modelAndView;
    }

    @GetMapping("/admin/comercios/{id}/products/top")
    public ModelAndView topProductsComercio(@PathVariable("id") int id){
        //todo check if id admin
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/topProductsComercio");
        //todo get data from service
        return modelAndView;
    }

    @GetMapping("/admin/comercios/{id}/exchangedPoints")
    public ModelAndView comercioExchangedPoints(@PathVariable("id") int id){
        //todo check if id admin
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comercioAfiliado/exchangedPoints");
        //todo get data from service
        modelAndView.addObject("comercio",comercioService.getComercio(id));
        modelAndView.addObject("total_points",comercioService.getComercioTotalPoints(id));
        return modelAndView;
    }



}
