package cr.tec.basesdedatos.bandera_azul.web;

import cr.tec.basesdedatos.bandera_azul.domain.Entities.AcumulatedPoints;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.CentroDeAcopio;
import cr.tec.basesdedatos.bandera_azul.domain.Entities.Usuario;
import cr.tec.basesdedatos.bandera_azul.service.CentroDeAcopioService;
import cr.tec.basesdedatos.bandera_azul.service.PersonaService;
import cr.tec.basesdedatos.bandera_azul.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@Controller
public class CentroDeAcopioController {
    @Autowired
    CentroDeAcopioService centroDeAcopioService;

    @Autowired
    PersonaService personaService;

    @Autowired
    UserService userService;

    @RequestMapping("/centros/")
    public ModelAndView showAllCentros(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/showAll");
        modelAndView.addObject("centros",centroDeAcopioService.getCentros());
        return modelAndView;
    }

    @RequestMapping("/centros/{id}")
    public ModelAndView showCentro(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/show");
        //todo call service
//        modelAndView.addObject("centro",centroDeAcopioService.getCentro(id));
        return modelAndView;
    }

    @GetMapping("/superadmin/centros/register")
    public ModelAndView registerCentro(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/register");
        modelAndView.addObject("centro", new CentroDeAcopio());
        List<Usuario> usuarios = new LinkedList<>(userService.getAllUsers().values());
        usuarios.sort(Comparator.comparing(Usuario::getUsername));
        modelAndView.addObject("usuarios", usuarios);
        return modelAndView;
    }

    @PostMapping("/superadmin/centros/register")
    public ModelAndView registerCentroSubmit(@ModelAttribute CentroDeAcopio centroDeAcopio) throws IOException {
        centroDeAcopioService.registerCentroAcopio(centroDeAcopio);
        return new ModelAndView("redirect:/centros/");
    }

    @RequestMapping("/admin/centros/{id}/recycle")
    public ModelAndView recycle(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/recycle");
        modelAndView.addObject("acumulatedPoints",new AcumulatedPoints());
        modelAndView.addObject("centro",centroDeAcopioService.getCentroAcopio(id));
        modelAndView.addObject("materials",centroDeAcopioService.getMaterials());
        modelAndView.addObject("unitTypes",centroDeAcopioService.getUnitTypes());
        List<Usuario> usuarios = new LinkedList<>(userService.getAllUsers().values());
        usuarios.sort(Comparator.comparing(Usuario::getUsername));
        modelAndView.addObject("usuarios", usuarios);
        return modelAndView;
    }

    @PostMapping("/admin/centros/{id}/recycle")
    public String recycleSubmit(@ModelAttribute AcumulatedPoints acumulatedPoints,
                                @PathVariable("id") int id_centro) throws IOException {
        acumulatedPoints.setId_centro(id_centro);
        centroDeAcopioService.registerRecyclation(acumulatedPoints);
        return "redirect:/admin/centros/"+ id_centro+"/recycles";
    }

    @RequestMapping("/admin/centros/{id}/recyclings")
    public ModelAndView showRecyclings(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/recyclings");
        //todo call service
//        modelAndView.addObject("centro",centroDeAcopioService.getCentro(id));
        return modelAndView;
    }

    @RequestMapping("/admin/centros/{id}/recyclings/byTypes")
    public ModelAndView showRecyclingsByType(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/recyclingsTypes");
        //todo call service
//        modelAndView.addObject("centro",centroDeAcopioService.getCentro(id));
        return modelAndView;
    }

    @RequestMapping("/admin/centros/{id}/recyclings/byUsers")
    public ModelAndView showRecyclingsByUsers(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/recyclingsUsers");
        //todo call service
//        modelAndView.addObject("centro",centroDeAcopioService.getCentro(id));
        return modelAndView;
    }

    @RequestMapping("/admin/centros/{id}/recyclings/topUsers")
    public ModelAndView showRecyclingsTopUsers(@PathVariable("id") int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("centroDeAcopio/recyclingTopUsers");
        //todo call service
//        modelAndView.addObject("centro",centroDeAcopioService.getCentro(id));
        return modelAndView;
    }


}
