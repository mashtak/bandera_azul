 //dropdown submenu inspired by https://stackoverflow.com/questions/44467377/bootstrap-4-multilevel-dropdown-inside-navigation
 $(document).ready(function() {
     $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
         if (!$(this).next().hasClass('show')) {
             $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
         }
         var $subMenu = $(this).next(".dropdown-menu");
         $subMenu.toggleClass('show');


         $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
             $('.dropdown-submenu .show').removeClass("show");
         });


         return false;
     });
 });


//animated counting
 $(document).ready(function() {
     $('.counter-count').each(function () {
         $(this).prop('Counter',0).animate({
             Counter: $(this).text()
         }, {
             duration: 5000,
             easing: 'swing',
             step: function (now) {
                 $(this).text(Math.ceil(now));
             }
         });
     });
 });


 function getDate(str){
     var parts = str.split("/");
     var date = new Date(parseInt(parts[2], 10),
         parseInt(parts[1], 10) - 1,
         parseInt(parts[0], 10));
     return date;
 }




